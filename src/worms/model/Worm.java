package worms.model;

import java.util.ArrayList;

import worms.model.entities.weapons.Bazooka;
import worms.model.entities.weapons.Rifle;
import worms.model.entities.weapons.Weapon;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of worm, with a position, orientation, mass and action points. It can also move and jump.
 * 
 * @invar 	All numbers must be valid
 * 		  | if(entity.isActive())
 * 		  | 	isValidRadius(getRadius()) == true
 * 		  |		getActionPointsCurrent() >= 0 && getActionPointsCurrent() <= getActionPointsMax() && !Double.isNaN(getActionPointsCurrent()) == false && !Double.isNaN(getActionPointsMax()) == false
 * 		  |		getHitPointsCurrent() >= 0 && getHitPointsCurrent() <= getHitPointsMax() && !Double.isNaN(getHitPointsCurrent()) == false && !Double.isNaN(getHitPointsMax()) == false
 * 		  |		radiusLowerBound >= 0 && !Double.isNaN(radiusLowerBound)
 * @invar	The name must be valid
 *  	  | if(entity.isActive())
 * 		  | 	isValidName(this.getName())
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class Worm extends JumpableEntity{

	private String name;

	private int actionPointsCurrent;
	private int hitPointsCurrent;

	private final static double radiusLowerBound = 0.25;

	private ArrayList<Weapon> weaponList;

	@Model
	private int selectedWeapon;

	private Team team;

	private Program program;
	
	/**
	 * Constructor for Worm. A worm is a JumpableEntity with a name, hit points (health), action points (to do stuff), a name
	 * and a Team.
	 * 
	 * This constructor makes a worm with the maximum amount of action points and hit points and without a team and without a program.
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the direction to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	name
	 * 			the name to initialize to
	 * @param 	world
	 * 			the world to initialize to
	 * 
	 * @effect	initialize as a Worm with given x, y position, radius, name and World and with the maximum amount of action points and hit points and
	 * 			with no team and program (null)
	 * 		  | this(x, y, direction, radius, name, 0, 0, true, true, world, null, null)
	 */
	@Raw
	protected Worm(double x, double y, double direction, double radius, String name, World world){
		this(x, y, direction, radius, name, world, null);
	}

	/**
	 * Constructor for Worm. A worm is a JumpableEntity with a name, hit points (health), action points (to do stuff), a name a
	 * team and a program.
	 * 
	 * This constructor makes a worm with the maximum amount of action points and hit points and without a team.
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the direction to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	name
	 * 			the name to initialize to
	 * @param 	world
	 * 			the world to initialize to
	 * @param	program	
	 * 			the program to initialize to
	 * 
	 * @effect	initialize as a Worm with given x, y position, radius, name and World and with the maximum amount of action points and hit points and
	 * 			with no team (null)
	 * 		  | this(x, y, direction, radius, name, 0, 0, true, true, world, null, program)
	 */
	@Raw
	protected Worm(double x, double y, double direction, double radius, String name, World world, Program program){
		this(x, y, direction, radius, name, 0, 0, true, true, world, null, program);
	}

	/**
	 * Constructor for Worm. A worm is a JumpableEntity with a name, hit points (health), action points (to do stuff), a name
	 * and a Team.
	 * 
	 * This constructor will add two rifles to the Worm: a Bazooka and a Rifle.
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the direction to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	name
	 * 			the name to initialize to
	 * @param 	actionPoints
	 * 			the action points to initialize to
	 * @param 	hitPoints
	 * 			the hit points to initialize to
	 * @param 	maxActionPoints
	 * 			initialize with the maximum amount of hit points?
	 * @param 	maxHitPoints
	 * 			initialize with the maximum amount of action points?
	 * @param 	world
	 * 			the world to initialize to
	 * @param 	Team
	 * 			the team to initialize to
	 * @param	program	
	 * 			the program to initialize to
	 * 
	 * @effect	initialize as a JumpableEntity with given x, y position, radius and World
	 * 		  | super(x,y, direction, radius, world)
	 * @effect	set the name to the given name
	 * 		  | this.setName(name)
	 * @effect	if maxActionPoints is true, initialize to the maximum amount of action points, otherwise initialize to the given actionPoints
	 * 		  | if(maxActionPoints)
	 * 		  |		this.setActionPointsCurrent(this.getActionPointsMax())
	 * 		  |	else
	 * 		  |		this.setActionPointsCurrent(actionPoints)
	 * @effect	if maxHitPoints is true, initialize to the maximum amount of hit points, otherwise initialize to the given hitPoints
	 * 		  | if(maxHitPoints)
	 * 		  |		this.setHitPointsCurrent(this.getHitPointsMax())
	 * 		  |	else
	 * 		  |		this.setHitPointsCurrent(hitPoints)
	 * @effect	set the team to the given team
	 * 		  | this.setTeam(team)
	 * @effect	Add a Rifle and a Bazooka to the weaponlist
	 * 		  | this.weaponList == new ArrayList<Weapon>()
	 * 		  | this.addWeapon(new Rifle(this))
	 * 		  |	this.addWeapon(new Bazooka(this))
	 * @effect	Set the program to the given program, if it is not null
	 * 		  |	if(program != null)
	 *		  |		this.setProgram(program);
	 *		  | 	program.setWorm(this);
	 * 
	 * @post	initialize this.radiusLowerBound to 0.25
	 * 		  |	new.radiusLowerBound == 0.25
	 * @post	initialize this.selectedWeapon to 0
	 * 		  |	new.selectedWeapon == 0
	 * 
	 * @note	The lower bound is set in the constructor. In future specifications it is possible that the lower 
	 * 			bound of the radius must be adjustable.
	 */
	@Raw
	private Worm(double x, double y, double direction, double radius, String name, int actionPoints, int hitPoints, boolean maxActionPoints, boolean maxHitPoints, World world, Team team, Program program){
		super(x,y, direction, radius, world);

		this.setName(name);

		if(maxActionPoints){
			this.setActionPointsCurrent(this.getActionPointsMax());
		} else{
			this.setActionPointsCurrent(actionPoints);
		}

		if(maxHitPoints){
			this.setHitPointsCurrent(this.getHitPointsMax());
		} else{
			this.setHitPointsCurrent(hitPoints);
		}

		this.setTeam(team);
		if(program != null){
			this.setProgram(program);
			program.setWorm(this);
		}

		this.weaponList = new ArrayList<Weapon>();
		this.selectedWeapon = 0;

		this.addWeapon(new Rifle(this));
		this.addWeapon(new Bazooka(this));
	}


	/**
	 * Get the lower bound on the radius of the Worm.
	 * 
	 * @return the lower bound on the radius
	 * 		 | result == this.radiusLowerBound
	 */
	@Basic @Immutable
	public static double getRadiusLowerBound() {
		return Worm.radiusLowerBound;
	}

	/**
	 * Get the name of the worm
	 * 
	 * @return the name of the worm
	 * 		 | result == this.name
	 */
	@Basic
	public String getName() {
		return name;
	}

	/**
	 * Returns the maximum amount of action points. This is the Mass rounded to the nearest integer.
	 * 
	 * @return 	The max amount of AP
	 * 			| result == round(this.getMass())
	 */
	public int getActionPointsMax() {
		return (int) Math.round(this.getMass());
	}

	/**
	 * Returns the maximum amount of hit points. In this implementation, the maximum amount of hit points
	 * is equal the maximum amount of action points.
	 * 
	 * @return 	The max amount of AP
	 * 			| result == round(this.getMass()) == this.getActionPointsMax()
	 */
	public int getHitPointsMax(){
		return (int) Math.round(this.getMass());
	}

	/**
	 * Returns the current amount of action points.
	 * 
	 * @return the current amount of action points
	 * 		 | result == this.actionPointsCurrent
	 */
	@Basic
	public int getActionPointsCurrent() {
		return this.actionPointsCurrent;
	}

	/**
	 * Returns the current amount of hit points.
	 * 
	 * @return the current amount of action points
	 * 		 | result == this.actionPointsCurrent
	 */
	@Basic
	public int getHitPointsCurrent() {
		return this.hitPointsCurrent;
	}

	/**
	 * Get the mass of this entity.
	 * 
	 * @return the mass of this entity
	 * 		 |	result == 1062 * (4/3)*Math.PI*Math.pow(this.getRadius(), 3)
	 */
	public double getMass(){
		return 1062.0 * (4.0/3.0)*Math.PI*Math.pow(this.getRadius(), 3.0);
	}

	/**
	 * Get the force that is exerted on this entity to let it jump.
	 * 
	 * @return	the force of the jump
	 * 		  | result == 5*this.getActionPointsCurrent() + this.getMass()*JumpableEntity.g
	 */
	@Override
	public double getJumpForce(){
		return 5*this.getActionPointsCurrent() + this.getMass()*JumpableEntity.g;
	}

	/**
	 * Get the list with the weapons of this Worm.
	 * 
	 * @return	the weapon list
	 * 		  | result == this.weaponList
	 */
	@Basic @Model
	private ArrayList<Weapon> getWeaponList(){
		return this.weaponList;
	}

	/**
	 * Get the Worm's selected weapon.
	 * 
	 * @return	the selected weapon.
	 * 		  |		result == return this.getWeaponList().get(this.selectedWeapon)
	 */
	protected Weapon getSelectedWeapon(){
		return this.getWeaponList().get(this.selectedWeapon);
	}

	/**
	 * Get the team this Worm is part of.
	 * 
	 * @return	the team this Worm is part of.
	 * 		  | result == this.team
	 */
	@Basic
	public Team getTeam(){
		return this.team;
	}

	/**
	 * Check if a given radius is valid.
	 * 
	 * @param 	radius
	 * 			radius to check
	 * 
	 * @return	returns true if all the conditions for a valid radius are met: these are the conditions
	 * 			stated in the superclass, and the radius must be larger than the lower bound.
	 * 		 | result == Entity.isValidRadius(radius) && radius > Worm.radiusLowerBound;
	 */
	@Override
	public boolean isValidRadius(double radius){
		return super.isValidRadius(radius) && radius >= Worm.getRadiusLowerBound();
	}

	/**
	 * Check if a given name is valid
	 * 
	 * @param 	name
	 * 			name to check
	 * 
	 * @return	returns true if all the conditions for a valid name are met: it cannot be null, it 
	 * 			must be at least 2 characters long, it must start with a capital letter and must 
	 * 			only consist of letters, quotes, digits and spaces
	 *		  |	isValid = true
	 * 		  |	if(name == null)
	 *		  |		isValid = false
	 *		  |	if(name.length() < 2)
	 *		  |		isValid = false
	 *		  |	if(!Character.isLetter(name.charAt(0)))
	 *		  |		isValid = false
	 * 		  |	if(!Character.isUpperCase(name.charAt(0)))
	 *		  |		isValid = false
	 *		  |	for(i = 0; i < name.length(); i++)
	 *		  |		if(!(Character.isLetter(name.charAt(i)) || (Character.isDigit(name.charAt(i))) || name.charAt(i) == ' ' || name.charAt(i) == '\'' || name.charAt(i) == '\"'))
	 *		  |			isValid = false
	 *		  |	result == isValid;
	 */
	private static boolean isValidName(String name){
		if(name == null)
			return false;
		if(name.length() < 2)
			return false;
		if(!Character.isLetter(name.charAt(0)))
			return false;
		if(!Character.isUpperCase(name.charAt(0)))
			return false;
		for(int i = 0; i < name.length(); i++)
			if(!(Character.isLetter(name.charAt(i)) || (Character.isDigit(name.charAt(i))) || name.charAt(i) == ' ' || name.charAt(i) == '\'' || name.charAt(i) == '\"'))
				return false;
		return true;
	}

	/**
	 * Check if this Worm can jump.
	 * 
	 * @param 	timeStepInterval
	 * 			the precision of the sampling of the jump curve used to calculated the in-jump positions
	 * 
	 * @return	returns true if this Worm is able to jump, false otherwise. The Worm needs enough action points
	 * 			and the distance between the position after the jump and the current position should be at least
	 * 			the length of the radius
	 *		  | if(!this.hasEnoughActionPointsToJump())
	 * 		  |		result == false
	 *		  |	jumpPosition = this.jumpStep(this.jumpTime(timeStepInterval))
	 *		  |	if(ModelUtil.distanceBetweenPositions(this.getPosition(), jumpPosition) < this.getRadius())
	 *		  |		result == false
	 * 		  |	else
	 *		  |		result == true
	 */
	public boolean canJump(double timeStepInterval){
		//The worm needs at least one action point
		if(!this.hasEnoughActionPointsToJump())
			return false;
		//Make sure the worm will land at least sigma meters from the current position
		Position jumpPosition = this.jumpStep(this.jumpTime(timeStepInterval));		
		if(ModelUtil.distanceBetweenPositions(this.getPosition(), jumpPosition) < this.getRadius())
			return false;
		//No problems, jumping is possible :)
		else
			return true;
	}

	/**
	 * Check if the Worm is able to move.
	 * 
	 * @return  returns true if the Worm is able to move. Therefore, the Worm needs enough action points.
	 * 			It could be that move uses a slightly different direction and 
	 * 			and it must be able to move at least 0.1m in a range between this.getDirection() +/- 0.7875 rad.
	 * 		  | if(!this.hasEnoughActionPointsToMove())
	 * 		  |		result == false
	 * 		  |	else
	 * 		  |		canMove = false
	 * 		  |		for(direction = this.getDirection() - 0.7875; direction <= this.getDirection() + 0.7875; direction += 0.0175)
	 * 		  |			x2 = this.getPosition().getX() + (this.getRadius() *(1+2e-6))*Math.cos(direction)
	 * 		  |			y2 = this.getPosition().getY() + (this.getRadius() *(1+2e-6))*Math.sin(direction)
	 * 		  |
	 * 		  |			newPosition = this.getWorld().getMaxMoveDistance(this, new Position(x2, y2))
	 * 		  |			distance = ModelUtil.distanceBetweenPositions(this.getPosition(), newPosition)
	 * 		  |
	 * 		  |			if(distance >= 0.1)
	 * 		  |				canMove = true
	 * 		  |
	 * 		  |		result == canMove
	 */
	public boolean canMove(){
		//Are there enough action points to move?
		if(!this.hasEnoughActionPointsToMove()){
			return false;
		}
		//Make sure the Worm can at least move 0.1 metres
		else {
			//Loop over all possible directions
			for(double direction = this.getDirection() - 0.7875; direction <= this.getDirection() + 0.7875; direction += 0.0175){
				double x2 = this.getPosition().getX() + (this.getRadius() *(1+2e-6))*Math.cos(direction);
				double y2 = this.getPosition().getY() + (this.getRadius() *(1+2e-6))*Math.sin(direction);

				Position newPosition = this.getWorld().getMaxMoveDistance(this, new Position(x2, y2));
				if(newPosition == null)
					continue;
				double distance = ModelUtil.distanceBetweenPositions(this.getPosition(), newPosition);

				if(distance >= 0.1){
					return true;
				}
			}

			return false;
		}
	}

	/**
	 * Check if the Worm is able to fall. A Worm can fall if it is not adjecant to impassable terrain.
	 * 
	 * @return  returns true if the Worm is able to fall. A Worm will fall if it is not adjacent to impassable terrain
	 * 		  | result == this.getWorld().isAdjacent(this.getPosition(),this.getRadius())
	 */
	public boolean canFall(){
		return !this.getWorld().isAdjacent(this.getPosition(),this.getRadius());
	}

	/**
	 * Check if the Worm can turn the given angle.
	 * 
	 * @param 	angle
	 * 			the angle to turn over
	 * 
	 * @return	true if the Worm can Turn the given angle. Therefore, angle must be a valid number
	 * 			and the Worm needs enough action points.
	 * 		  | if(Double.isNaN(angle))
	 * 		  |		result == false
	 * 		  | else
	 * 		  |		result == this.hasEnoughActionPointsToTurn(angle)
	 */
	public boolean canTurn(double angle){
		if(Double.isNaN(angle)){
			return false;
		} else{
			return this.hasEnoughActionPointsToTurn(angle);
		}
	}

	/**
	 * Check if the Worm can shoot.
	 * 
	 * @return	true if the Worm can shoot. The Worm needs enough action points to shoot with the currently selected weapon.
	 * 		  | result == this.hasEnoughActionPointsToShoot()
	 */
	public boolean canShoot(){
		return this.hasEnoughActionPointsToShoot();
	}
	
	/**
	 * Has this Worm enough action points to turn?
	 * 
	 * @param	angle
	 * 			the angle to turn over
	 * 
	 * @return	true if this Worm has enough action points, false otherwise
	 * 		  |	result == this.getActionPointsCurrent() >= (int) Math.ceil(Math.abs(60*(angle/(2*Math.PI))))
	 */
	public boolean hasEnoughActionPointsToTurn(double angle){
		return this.getActionPointsCurrent() >= (int) Math.ceil(Math.abs(60*(angle/(2*Math.PI))));
	}
	
	/**
	 * Has this Worm enough action points to move?
	 * 
	 * We test here if the Worm has enough action points to move in the direction it currently faces, however,
	 * it could be than the Worm moves in a slightly different direction. See move() for more details.

	 * @return	true if this Worm has enough action points, false otherwise
	 * 		  |	result == (this.getActionPointsCurrent() - this.moveActionPointCost(this.getDirection())) < 0
	 */
	public boolean hasEnoughActionPointsToMove(){
		return this.getActionPointsCurrent() >= this.moveActionPointCost(this.getDirection());
	}
	
	/**
	 * Has this Worm enough action points to jump?

	 * @return	true if this Worm has enough action points, false otherwise
	 * 		  |	result == this.getActionPointsCurrent() == 0 ? false : true
	 */
	public boolean hasEnoughActionPointsToJump(){
		return this.getActionPointsCurrent() != 0;
	}
	
	/**
	 * Has this Worm enough action points to shoot?

	 * @return	true if this Worm has enough action points, false otherwise
	 * 		  |	result == (this.getActionPointsCurrent() - this.getSelectedWeapon().getActionPointCost()) >= 0
	 */
	public boolean hasEnoughActionPointsToShoot(){
		return this.getActionPointsCurrent() >= this.getSelectedWeapon().getActionPointCost();
	}

	/**
	 * Change the position of the Worm. The Worm will move in the given direction, with a maximum
	 * moved distance equal to the current radius. The direction the Worm moves lies in the interval
	 * this.getDirection() +/- 0.0175 rad. 
	 * 
	 * To decide which direction to move, the algorithm uses a coefficient. We need to maximize the distance, 
	 * minimize the divergence and give priority to moves to positions adjacent to terrain. Therefore, the
	 * coefficient is calculated as JB = isAdjacentCoof*Distance/(1+Divergence). A large distance and a small 
	 * divergence will make the coefficient large. The isAdjacentCoof is 2 if the position we move to is adjacent, 
	 * otherwise it is 1. This coefficient is calculated for each direction in the interval, and the direction 
	 * with the largest coefficient is chosen.
	 * 
	 * @post	the direction that is moved in has the largest coefficient in the interval
	 * 		  | finalDirection = Math.atan((this.getPosition().getY() - new.getPosition().getY())/(this.getPosition().getX() - new.getPosition().getX()))
	 * 		  |	JBFinal = (((this.getWorld().isAdjacent(new.getPosition(), this.getRadius()))?2:1)
	 * 		  |					*ModelUtil.distanceBetweenPositions(this.getPosition(), new.getPosition()))
	 * 		  |					/(1+abs(this.getDirection() - finalDirection))
	 * 		  |
	 * 		  | for( i = 0; i <= 90; i++)
	 * 		  | 	if(i % 2 == 0)
	 * 		  |			div = 0.0175*(i/2)
	 * 		  |			direction = this.getDirection() + div
	 * 		  |		else
	 * 		  |			div = 0.0175*((i+1)/2)
	 * 		  |			direction = this.getDirection() - div
	 * 		  |		double x2 = this.getPosition().getX() + (this.getRadius() *(1+2e-6))*Math.cos(direction)
	 * 		  |		double y2 = this.getPosition().getY() + (this.getRadius() *(1+2e-6))*Math.sin(direction)
	 * 		  |		Position newPosition = this.getWorld().getMaxMoveDistance(this, new Position(x2, y2))
	 * 		  |		distance = ModelUtil.distanceBetweenPositions(this.getPosition(), newPosition)
	 * 		  |		double isAd = (this.getWorld().isAdjacent(newPosition, this.getRadius()))?2:1
	 * 		  |
	 * 		  |		JB = isAd*distance/(1+div)
	 * 		  |
	 * 		  |		JB <= JBFinal
	 * @post	the action points are decreased based on the final direction (see moveActionPointCost() for more details)
	 * 		  |	finalDirection = Math.atan((this.getPosition().getY() - new.getPosition().getY())/(this.getPosition().getX() - new.getPosition().getX()))
	 * 		  | new.getActionPointsCurrent() == this.getActionPointsCurrent() - moveActionPointCost(finalDirection))
	 * 
	 * @throws	IllegalMovementException
	 * 			the worm is not able to move
	 * 		  | !canMove()
	 * 		  
	 */
	protected void move() throws IllegalMovementException{
		if(!canMove())
			throw new IllegalMovementException("Not enough action points for this movement");

		double finalDistance = 0;
		double finalDirection = 0;
		double finalJB = 0;
		Position finalPosition = null;

		double distance;
		double direction;
		double div;
		double JB;

		//Loop over all possible directions
		for(int i = 0; i <= 90; i++){
			//When i is even, we inspect the left side of the current direction, otherwise the right side
			if(i % 2 == 0){
				div = 0.0175*(i/2);
				direction = this.getDirection() + div;
			} else{
				div = 0.0175*((i+1)/2);
				direction = this.getDirection() - div;
			}

			//First find the longest possible distance from the current position
			double x2 = this.getPosition().getX() + (this.getRadius() *(1+2e-6))*Math.cos(direction);
			double y2 = this.getPosition().getY() + (this.getRadius() *(1+2e-6))*Math.sin(direction);

			//Search a position in a straight line that is passable
			Position newPosition = this.getWorld().getMaxMoveDistance(this, new Position(x2, y2));
			if(newPosition == null)
				continue;
			distance = ModelUtil.distanceBetweenPositions(this.getPosition(), newPosition);
			double isAd = (this.getWorld().isAdjacent(newPosition, this.getRadius()))?2:1;

			//We need to maximize the distance, minimize the divergence and give priority to moves
			//to positions adjacent to terrain. Therefore  we have chosen for a coefficient that reflects
			//this:
			// 		JB = isAdjacentCoof*Distance/(1+Divergence)
			//A large distance and a small divergence will make the coefficient large. The isAdjacentCoof is 2
			//if the position we move to is adjacent, otherwise it is 1
			JB = isAd*distance/(1+div);
			if(finalJB < JB){
				finalJB = JB;
				finalPosition = newPosition;
				finalDirection = direction;
				finalDistance = distance;
			}
		}

		this.setPosition(finalPosition);

		this.setActionPointsCurrent(this.getActionPointsCurrent() - moveActionPointCost(finalDirection));

		//See if we can eat something
		this.eat();
	}

	/**
	 * Let the Worm fall down. This method is called after every move (move, jump) and will let the 
	 * Worm fall down if it is not adjecant to impassable terrain.
	 * 
	 * @post	the Worm is adjecant to impassable terrain after a fall
	 * 		  | this.getWorld().isAdjecant(new.getPosition(), this.getRadius()) == true
	 * @post	the hitpoints are decreased with 3 times the fallen distance
	 * 		  |	new.getHitpointsCurrent() == this.getHitPointsCurrent() - (int)(3*ModelUtil.distanceBetweenPositions(this.getPosition(), new.getPosition())
	 * 
	 * @throws	IllegalMovementException
	 * 			the worm is not able to fall
	 * 		  | !canFall()
	 * 		  
	 */
	protected void fall() throws IllegalMovementException{
		if(!this.canFall())
			throw new IllegalMovementException("No room to fall!");

		//The position this Worm will have after falling
		Position newPosition = this.getWorld().getMaxFallDistance(this);

		double fallingDistance = ModelUtil.distanceBetweenPositions(this.getPosition(), newPosition);

		this.setPosition(newPosition);

		this.setHitPointsCurrent(this.getHitPointsCurrent() - (int)(3*fallingDistance));

		//See if we can eat something
		this.eat();
	}

	/**
	 * If there is food at the current location of the worm, eat it.
	 * 
	 * @post	for each piece of food that collides with the Worm at it's current location, 
	 * 			the Worm's radius increased with 10%
	 * 		  |	foodCount = 0
	 * 		  |	for(Food nextFood : this.getWorld().getFoods())
	 * 		  |		if(ModelUtil.isCollision(this, nextFood))
	 * 		  |			foodCount++;
	 * 		  |	new.getRadius() == Math.pow(1.10, foodCount)*this.getRadius()
	 * @post	there are no pieces of food that collide with the Worm at it's current location
	 * 		  |	for(Food nextFood : this.getWorld().getFoods())
	 * 		  |		ModelUtil.isCollision(new, nextFood) == false
	 */
	protected void eat(){
		if(!this.isActive())
			return;
		ArrayList<Food> foodToDestroy = new ArrayList<Food>();

		for(Food nextFood : this.getWorld().getFoods()){
			//Are we colliding with a food object?
			if(ModelUtil.isCollision(this, nextFood)){
				//Increase radius with 10
				this.setRadius(this.getRadius()*1.10);

				//Remove the food object from the world
				foodToDestroy.add(nextFood);
			}
		}

		//Otherwise we get an ConcurrentIterationException
		while(foodToDestroy.size() != 0){
			foodToDestroy.get(0).destroy();
			foodToDestroy.remove(0);
		}
	}

	/**
	 * Change the orientation of the Worm over the given angle. Turning cost a certain
	 * amount of action points.
	 * 
	 * @param 	angle
	 * 			the angle to turn over
	 * 
	 * @pre		the angle must be valid
	 * 		  | canTurn(angle) == true
	 * 
	 * @effect	the direction will be increased by angle modulus 2*Pi, and will be made positive if it was negative
	 * 		  | newDirection = (this.getDirection() + angle) % (2*Math.PI)
	 * 		  |	newDirection = (newDirection < 0)?newDirection+2*Math.PI:newDirection
	 * 		  | new.getDirection() == newDirection
	 * 
	 * @effect the action points will be deminished acording to the turned angle
	 * 		  |	new.getActionPointsCurrent() == this.getActionPointsCurrent() - Math.ceil(Math.abs(60*(angle/(2*Math.PI))))
	 */
	protected void turn(double angle){
		assert(canTurn(angle));

		//Turn
		double direction = (this.getDirection() + angle) % (2*Math.PI);
		direction = (direction < 0)?direction+2*Math.PI:direction;
		this.setDirection(direction);

		//Calculate used action points
		int actionPointsUsed = (int) Math.ceil(Math.abs(60*(angle/(2*Math.PI))));
		this.setActionPointsCurrent(this.getActionPointsCurrent() - actionPointsUsed);
	}

	/**
	 * Let this Worm jump. It will position the Worm to the calculated after-jump location.
	 * 
	 * @param	timeStepInterval
	 * 			The precision the calculations are done with. 
	 * 
	 * @effect	jump as defined in the JumpableEntity
	 *    	  | super.jump(timeStepInterval)
	 * @effect	set the action points to zero
	 * 		  | new.getActionPointsCurrent() == 0
	 * @effect	eat all available food pieces at the landing location
	 *    	  |	this.eat()
	 *    
	 * @throws	IllegalMovementException
	 * 			This entity cannot jump (see super class)
	 * 		  | !this.canJump(timeStepInterval)
	 */
	@Override
	protected void jump(double timeStepInterval) throws IllegalMovementException{
		super.jump(timeStepInterval);

		this.setActionPointsCurrent(0);

		//See if we can eat something
		this.eat();
	}

	/**
	 * Let this Worm shoot with the given propulsion yield. The Worm fires with it's
	 * currently selected weapon. Shooting a weapon requires a certain amount of action
	 * points. These action points will be diminished from the current amount of action
	 * points.
	 * 
	 * @param	propulsionYield
	 * 			the propulsion yield to shoot with
	 * 
	 * @effect	shoot the selected weapon
	 *    	  | this.getSelectedWeapon().fire(propulsionYield)
	 * @effect	set the action points to zero
	 * 		  | new.getActionPointsCurrent() == 0
	 * @post	diminish the correct amount of action points
	 *    	  |	new.getActionPointsCurrent() == this.getActionPointsCurrent() - this.getSelectedWeapon().getActionPointCost()
	 *    
	 * @throws	IllegalShootException
	 * 			This entity cannot shoot
	 * 		  | !this.canShoot(propulsionYield)
	 * @throws	IllegalArgumentException
	 * 			There was a problem in the fire method of the selected Weapon
	 * 		  | this.getSelectedWeapon().fire(propulsionYield)
	 */
	protected void shoot(int propulsionYield) throws IllegalShootException, IllegalArgumentException{
		if(!this.canShoot())
			throw new IllegalShootException("The worm needs enough action points");

		this.getSelectedWeapon().fire(propulsionYield);

		this.setActionPointsCurrent(this.getActionPointsCurrent() - this.getSelectedWeapon().getActionPointCost());
	}

	/**
	 * Calculate the amount it would cost to move along the given direction.
	 * 
	 * @param 	direction
	 * 			the direction the Worm will move
	 * 
	 * @return  the amount of action points it would cost to move along this direction
	 * 		  | result == (int) Math.ceil(Math.abs(Math.cos(direction)) + Math.abs(4*Math.sin(direction)))
	 */
	@Model
	private int moveActionPointCost(double direction){
		double horizontalStepCost = Math.cos(direction);
		double verticalStepCost = Math.sin(direction);

		return (int) Math.ceil(Math.abs(horizontalStepCost) + Math.abs(4*verticalStepCost));
	}

	/**
	 * Called everytime it's this worms' turn. It sets the current amount of action points
	 * to the maximum amount and increases the hit points with 10.
	 * 
	 * @effect	the action points are set to max
	 * 		  |	this.setActionPointsCurrent(this.getActionPointsMax())
	 * @effect	the hit points are increased with 10
	 * 		  |	this.setHitPointsCurrent(this.getHitPointsCurrent() + 10)
	 */
	protected void playTurn(){
		this.setActionPointsCurrent(this.getActionPointsMax());
		this.setHitPointsCurrent(this.getHitPointsCurrent() + 10);
		if(program != null)
			program.execute();
	}

	/**
	 * Add a weapon to the Worm's arsenal.
	 * 
	 * @param	weapon
	 * 			the weapon to add to the Worms weapon list
	 * 
	 * @post	the Worm's weapon list contains the added weapon
	 * 		  |	new.getWeaponList().contains(weapon) == true
	 */
	@Raw
	private void addWeapon(Weapon weapon){
		this.weaponList.add(weapon);
	}

	/**
	 * Select the next weapon in the Worm's weapon list.
	 * 
	 * @post	the index that indicates the currently selected weapon is increased
	 * 			if the maximum index is not yet reached. Otherwise it is set to 0.
	 * 		  |	if(this.selectedWeapon < this.getWeaponList().size() - 1)
	 * 		  |		new.selectedWeapon == this.selectedWeapon += 1
	 * 		  |	else
	 * 		  |		new.selectedWeapon == 0
	 */
	protected void selectNextWeapon(){
		if(this.selectedWeapon < this.getWeaponList().size() - 1){
			this.selectedWeapon += 1;
		} else{
			this.selectedWeapon = 0;
		}
	}

	/**
	 * Set the name of the worm
	 * 
	 * @param 	name
	 * 			the name to set to
	 * 
	 * @post	the name of the worm is set to name
	 * 		  | new.getName() == name
	 * 
	 * @throws	IllegalArgumentException
	 * 			The name of the worm is not valid
	 * 		  | !Worm.isValidName(name)
	 */
	@Raw
	protected void setName(String name) throws IllegalArgumentException {
		if(!Worm.isValidName(name))
			throw new IllegalArgumentException("the name given was invalid");	
		this.name = name;
	}

	/**
	 * Sets the current amount of action points.
	 * 
	 * @param 	actionPointsCurrent
	 *			the amount of action points to set to
	 *
	 * @post 	If the given amount of action points are not negative 
	 * 			and smaller than the maximum amount, then this worms action points
	 * 			are equal to the given amount
	 * 		  | if(0 <= actionPointsCurrent && actionPointsCurrent <= this.getActionPointsMax())
	 * 		  | 	new.actionPointsCurrent == actionPointsCurrent
	 * @post	If the given amount of action points is negative, this worms
	 * 			action points are equal to 0
	 * 		  | if (actionPointsCurrent < 0)
	 * 		  | 	new.actionPointsCurrent == 0
	 * @post 	If the given amount of action points exceeds the maximum amount
	 *			then this worms action points are equal to the maximum amount
	 *		  | if(actionPointsCurrent > this.getActionPointsMax())
	 *		  | 	new.actionPointsCurrent == this.getActionPointsMax()
	 */
	@Raw
	protected void setActionPointsCurrent(int actionPointsCurrent) {
		if(actionPointsCurrent < 0)
			this.actionPointsCurrent = 0;
		else if(actionPointsCurrent > this.getActionPointsMax())
			this.actionPointsCurrent = this.getActionPointsMax();
		else
			this.actionPointsCurrent = actionPointsCurrent;
	}

	/**
	 * Sets the current amount of hit points.
	 * 
	 * @param 	hitPointsCurrent
	 *			the amount of action points to set to
	 *
	 * @post 	If the given amount of hit points are not negative 
	 * 			and smaller than the maximum amount, then this worms hit points
	 * 			are equal to the given amount
	 * 		  | if(0 <= hitPointsCurrent && hitPointsCurrent <= this.getHitPointsMax())
	 * 		  | 	new.hitPointsCurrent == hitPointsCurrent
	 * @post	If the given amount of hit points is negative, this worms
	 * 			hit points are equal to 0
	 * 		  | if (hitPointsCurrent < 0)\
	 * 		  | 	new.hitPointsCurrent == 0
	 * @post 	If the given amount of hit points exceeds the maximum amount
	 *			then this worms hit points are equal to the maximum amount
	 *		  | if(hitPointsCurrent > this.getHitPointsMax())
	 *		  | 	new.hitPointsCurrent == this.getHitPointsMax()
	 */
	@Raw
	protected void setHitPointsCurrent(int hitPointsCurrent) {
		if(hitPointsCurrent <= 0){
			this.hitPointsCurrent = 0;
			this.destroy();
		}
		else if(hitPointsCurrent > this.getHitPointsMax()){
			this.hitPointsCurrent = this.getHitPointsMax();
		}
		else{
			this.hitPointsCurrent = hitPointsCurrent;
		}
	}

	/**
	 * Set the current radius of the Worm
	 * 
	 * @param 	radius
	 * 			The radius to set to.
	 * 
	 * @effect	set the radius with the super class's setRadius(radius) method
	 * 		  | super.setRadius(radius)
	 * 
	 * @effect	changing the radius changes the max amount of AP, thus we may need to reset the action points
	 * 		  | this.setActionPointsCurrent(this.getActionPointsCurrent())
	 * 
	 * @throws	IllegalArgumentException
	 * 			the radius must be a valid number
	 * 		  | !isValidRadius(radius) == true
	 */
	@Override @Raw
	protected void setRadius(double radius) throws IllegalArgumentException{
		super.setRadius(radius);

		this.setActionPointsCurrent(this.getActionPointsCurrent());
	}

	/**
	 * Set the team of the Worm.
	 * 
	 * @param 	team
	 * 		  | the team to set to
	 * 
	 * @post	the team of this Worm is set to team
	 * 		  |	new.team == team;
	 */
	protected void setTeam(Team team){
		this.team = team;
	}

	/**
	 * @return the program
	 */
	public Program getProgram() {
		return program;
	}

	/**
	 * @param program the program to set
	 */
	protected void setProgram(Program program) {
		this.program = program;
	}

	@Override
	public String toString(){
		return getName();
	}
}
