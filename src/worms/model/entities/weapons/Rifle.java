package worms.model.entities.weapons;

import be.kuleuven.cs.som.annotate.Raw;
import worms.model.Projectile;
import worms.model.Worm;

/**
 * A Rifle where Worms can shoot with. 
 * 
 * @invar 	The action point cost must be 10
 * 		  | getActionPointCost() == 10
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Rifle extends Weapon{
	
	private static final String _NAME = "Rifle";

	/**
	 * Constructor for a Rifle.
	 * 
	 * @param 	worm
	 * 			the worm this Rifle belongs to
	 * 
	 * @effect 	initialize as a Weapon with name "Rifle" and the given worm as owner
	 * 		  | super("Rifle", worm)
	 * @effect	set the action points needed to shoot this rifle to 10
	 * 		  | this.setActionPointCost(10)
	 */
	@Raw
	public Rifle(Worm worm) {
		super(Rifle._NAME, worm);
		
		this.setActionPointCost(10);
	}
	
	/**
	 * Fire this Rifle. A projectile will be created which has a jumpForce of 1.5.
	 * 
	 * @param 	propulsionYield
	 * 			this parameter is ignored.
	 * 
	 * @effect	a projectile is created with the owner of this Weapon as Worm, a mass of 0.010, 
	 * 			a force of 1.5 and with a hit point cost of 20
	 * 		  |	Projectile.createProjectile(this.getWorm(), 0.010, 1.5, 20)
	 */
	@Override
	public void fire(double propulsionYield){		
		double force = 1.5;
		Projectile.createProjectile(this.getWorm(), 0.010, force, 20);
	}

}
