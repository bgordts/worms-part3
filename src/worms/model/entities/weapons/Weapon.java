package worms.model.entities.weapons;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import worms.model.Worm;

/**
 * Abstract class representing a Weapon. A Weapon has a name, requires a certain amount of
 * action points to shoot, and belongs to a Worm.
 * 
 * @invar 	The name may not be null
 *		  | getName() != null
 * @invar 	There must be a Worm to whom this weapon belongs
 * 		  | getWorm() != null
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public abstract class Weapon {
	
	private String name;
	private Worm worm;
	private int actionPointCost;

	/**
	 * Constructor for Weapon.
	 * 
	 * @param 	name
	 * 			the name to initialize to
	 * @param 	worm
	 * 			the worm this Weapon belongs to
	 * 
	 * @effect 	set the name of this weapon to the given name
	 * 		  | this.setName(name)
	 * @effect	set the owning Worm to the given worm
	 * 		  | this.setWorm(worm)
	 */
	@Raw
	public Weapon(String name, Worm worm) {
		this.setName(name);
		this.setWorm(worm);
	}
	
	/**
	 * Get the name of this Weapon.
	 * 
	 * @return	the name of the Weapon
	 * 		  |	result == this.name
	 */
	@Basic
	public String getName() {
		return name;
	}

	/**
	 * Get owning Worm of this Weapon.
	 * 
	 * @return	the Worm that owns this Weapon
	 * 		  |	result == this.worm
	 */
	@Basic
	protected Worm getWorm() {
		return worm;
	}
	
	/**
	 * Get the action point cost to shoot this Weapon.
	 * 
	 * @return	the Worm that owns this Weapon
	 * 		  |	result == this.worm
	 */
	@Basic
	public int getActionPointCost(){
		return actionPointCost;
	}
	
	/**
	 * Returns true if this Weapon can be fired with the given propulsion yield.
	 * 
	 * @param 	propulsionYield
	 * 			the propulsion yield to shoot with
	 * 
	 * @return	true if this weapon can be fired with the given propulsion yield. Therefore, the propulsion
	 * 			yield should lie between 0 and 100
	 * 		  |	result == 0 <= propulsionYield && propulsionYield <= 100
	 */
	public boolean canFire(double propulsionYield){
		return 0 <= propulsionYield && propulsionYield <= 100;
	}
	
	/**
	 * Fire this weapon.
	 * 
	 * @param 	propulsionYield
	 * 			the propulsion yield to shoot with
	 * 
	 * @throws	IllegalArgumentException
	 * 			The propulsion yield is not valid
	 */
	public abstract void fire(double propulsionYield) throws IllegalArgumentException;

	/**
	 * Set the owner of this Weapon.
	 * 
	 * @param 	worm
	 * 			the Worm that owns this Weapon
	 * 
	 * @effect	the owner of this Weapon is set to the given worm
	 * 		  |	new.getWorm() == worm
	 */
	@Raw
	protected void setWorm(Worm worm) {
		this.worm = worm;
	}

	/**
	 * Set the name of this Weapon.
	 * 
	 * @param 	name
	 * 			the name of this Weapon
	 * 
	 * @effect	the name of this Weapon is set to the given name
	 * 		  |	new.getName() == name
	 */
	@Raw
	protected void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Set the cost to shoot this Weapon.
	 * 
	 * @param 	actoinPointCost
	 * 			the cost to shooot this Weapon
	 * 
	 * @effect	the cost to shoot this weapon is set to the given actionPointCost
	 * 		  |	new.getActionPointCost() == actoinPointCost
	 */
	@Raw
	protected void setActionPointCost(int actoinPointCost){
		this.actionPointCost = actoinPointCost;
	}
	
	
}
