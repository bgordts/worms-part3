package worms.model.entities.weapons;

import be.kuleuven.cs.som.annotate.Raw;
import worms.model.Projectile;
import worms.model.Worm;

/**
 * A Bazooka where Worms can shoot with. 
 * 
 * @invar 	The action point cost must be 50
 * 		  | getActionPointCost() == 50
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Bazooka extends Weapon{
	
	private static final String _NAME = "Bazooka";

	/**
	 * Constructor for a Bazooka.
	 * 
	 * @param 	worm
	 * 			the worm this Bazooka belongs to
	 * 
	 * @effect 	initialize as a Weapon with name "Bazooka" and the given worm as owner
	 * 		  | super("Bazooka", worm)
	 * @effect	set the action points needed to shoot this bazooka to 50
	 * 		  | this.setActionPointCost(50)
	 */
	@Raw
	public Bazooka(Worm worm) {
		super(Bazooka._NAME, worm);
		
		this.setActionPointCost(50);
	}
	
	/**
	 * Fire this Bazooka. A projectile will be created which has a jumpForce depending on the propulsion yield
	 * 
	 * @param 	propulsionYield
	 * 			the propulsion yield to shoot with
	 * 
	 * @effect	a projectile is created with the owner of this Weapon as Worm, 
	 * 			a mass of 0.3, a force between 2.5 and 7 depending on the propulsion yield 
	 * 			and with a hit point cost of 80
	 * 		  |	Projectile.createProjectile(this.getWorm(), this.getWorm().getWorld(), 0.3, 2.5 + (100/7)*propulsionYield, 80)
	 * 
	 * @throws	IllegalArgumentException
	 * 			The propulsion yield is not valid
	 * 		  | this.canFire(propulsionYield)
	 */
	@Override
	public void fire(double propulsionYield) throws IllegalArgumentException{
		if(!this.canFire(propulsionYield))
			throw new IllegalArgumentException("Propulsion yield must be a integer between 0 and 100.");
		
		double force = 2.5 + (7.0/100.0)*propulsionYield;
		Projectile.createProjectile(this.getWorm(), 0.3, force, 80);
	}

}
