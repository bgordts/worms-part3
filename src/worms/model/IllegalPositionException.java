package worms.model;

/**
 * Exception thrown when an illegal position is encountered.
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class IllegalPositionException extends RuntimeException {

	IllegalPositionException(Entity e,String message){
		super(message);
		e.destroy();
	}
	
	private static final long serialVersionUID = -3603958343513628764L;

}
