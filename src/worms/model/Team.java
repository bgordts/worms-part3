package worms.model;

import java.util.HashSet;

import be.kuleuven.cs.som.annotate.*;


/**
 * A class to represent a team of worms
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Team {
	
	//The worms in the team
	private HashSet<Worm> team;
	
	//The name of the team
	private final String name;
	
	/**
	 * Make a new team
	 * 
	 * @param name the name to set to
	 * 
	 * @throws IllegalArgumentException the name given must be valid
	 * 		 | !isValidName(name)
	 */
	protected Team(String name) throws IllegalArgumentException{
		if(!isValidName(name))
			throw new IllegalArgumentException("Name given is not valid");
		this.name = name;
		team = new HashSet<Worm>();
	}

	/**
	 * Get the name of the team
	 * 
	 * @return the name of this team
	 */
	@Basic @Immutable
	public String getName() {
		return name;
	}
	
	/**
	 * Check if the name is valid
	 * 
	 * @param name the name to check
	 * 
	 * @post  the name must not be null
	 * 		| if(result == null)
	 * 		|	return false
	 * @post  the name at least 2 characters long
	 * 		| if(name.length()<2)
	 *		|	result == false
	 * @post  The name must start with an upper case letter
	 * 		| if(!Character.isUpperCase(name.charAt(0)))
	 * 		| 	result == false
	 * @post  Each character must be a letter
	 * 		| for each c in name
	 * 		|	if(!(Character.isUpperCase(c) || Character.isLowerCase(c)))
	 * 		|		result == false
	 * @return If the name has successfully withstood each test it is valid
	 * 		| result == true
	 */
	@Model
	private static boolean isValidName(String name){
		if(name == null)
			return false;
		if(name.length()<2)
			return false;
		if(!Character.isUpperCase(name.charAt(0)))
			return false;
		for(int i = 0; i < name.length(); i++){
			char c = name.charAt(i);
			if(!(Character.isUpperCase(c) || Character.isLowerCase(c)))
				return false;
		}
		return true;
		
	}

	/** Adds a worm to the team
	 * 
	 * @param worm the worm to add
	 * 
	 * @post the worm is added to the team
	 * 		| this.getTeam().add(worm)
	 * 		| worm.setTeam(this)
	 */
	protected void add(Worm worm){
		this.getTeam().add(worm);
		worm.setTeam(this);
	}

	/**
	 * Returns all the members of this team, living or dead
	 * 
	 * @return the team
	 * 		| result == this.team
	 */
	@Basic
	@SuppressWarnings("unchecked")
	public HashSet<Worm> getTeam() {
		return (HashSet<Worm>) this.team.clone();
	}
	
	/**
	 * Get all live members of the team
	 * 
	 * @return live members of the team
	 * 		| for each worm in this.getTeam()
	 * 		|	if(worm.isActive())
	 * 		|		result.add(worm)
	 */
	public HashSet<Worm> getLive() {
		HashSet<Worm> live = new HashSet<Worm>();
		
		for(Worm worm : this.getTeam()){
			if(worm.isActive())
				live.add(worm);
		}
		
		return live;
	}

}
