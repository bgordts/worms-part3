package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * A projectile, shot with a Weapon. A projectile 'jumps' from a gun and when it collides
 * with a Worm, it causes a certain amount of damage.
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Projectile extends JumpableEntity{

	private final static double DENSITY = 7800;
	//The distance that is added to the x and y position of this projectile to make sure it does not collide with the worm 
	//on creation.
	private final static double EXTRA_DISTANCE = 1e-6;

	private double mass;
	private double jumpForce;
	private int hitPointCost;

	/**
	 * Constructor for Projectile.
	 * 
	 * @Note	this constructor is private, because a Projectile shoould be created with the
	 * 			Projectile.createProjectile() method
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the direction to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	world
	 * 			the world where this projectile is in
	 * @param 	mass
	 * 			the mass to initialize to
	 * @param 	force
	 * 			the jump force to initialize to
	 * @param 	hitPointCost
	 * 			the amount of hit points a Worm loses when it is hit by this projectile
	 * 
	 * @effect	initialize as a JumpableEntity with the given, x and y position, radius and world
	 * 		  | super(x, y, direction, radius, world)
	 * @effect	the mass will be set to the given mass
	 * 		  | this.setMass(mass)
	 * @effect	the jump force will be set to the given force
	 * 		  | this.setForce(force)
	 * @effect	the hit point cost of this projectile will be set to the given hitPointCost
	 * 		  | this.setHitPointCost(hitPointCost)
	 * @effect	let the world know this is the current active projectile
	 * 		  | this.getWorld().setActiveProjectile(this)
	 */
	@Raw
	private Projectile(double x, double y, double direction, double radius, World world, double mass, double force, int hitPointCost) {
		super(x, y, direction, radius, world);

		this.setMass(mass);
		this.setJumpForce(force);
		this.setHitPointCost(hitPointCost);
	}

	/**
	 * Creates a new projectile.
	 * 
	 * @param 	worm
	 * 			the worm that shoots this projectile
	 * @param 	mass
	 * 			the mass to initialize to
	 * @param 	force
	 * 			the jump force to initialize to
	 * @param 	hitPointCost
	 * 			the amount of hit points a Worm loses when it is hit by this projectile
	 * 
	 * @return	a new Projectile which is located just outside the Worm's sphere at the direction
	 * 			the worm is facing, it's direction is the same as the directino of the Worm, the
	 * 			radius is based on the given mass and a density of 7800, it's world is the same as
	 * 			the given worm's world, it's mass, force and hit point cost are the given one's
	 * 		  |	radius = Math.pow(mass*3/(Projectile._DENSITY*4*Math.PI), 1.0 / 3.0)
	 * 		  |	dirCos = Math.cos(worm.getDirection())
	 * 		  |	dirSin = Math.sin(worm.getDirection())
	 * 		  |	posX = worm.getPosition().getX() + dirCos*worm.getRadius() + dirCos*radius + Math.copySign(_EXTRA_DINSTANCE, dirCos)
	 * 		  |	posY = worm.getPosition().getY() + dirSin*worm.getRadius() + dirSin*radius + Math.copySign(_EXTRA_DINSTANCE, dirSin)
	 * 		  |	result == new Projectile(posX, posY, worm.getDirection(), radius, worm.getWorld(), mass, force, hitPointCost)
	 */
	public static Projectile createProjectile(Worm worm, double mass, double force, int hitPointCost){		
		double radius = Math.pow(mass*3/(Projectile.DENSITY*4*Math.PI), 1.0 / 3.0);

		double dirCos = Math.cos(worm.getDirection());
		double dirSin = Math.sin(worm.getDirection());

		double posX = worm.getPosition().getX() + dirCos*worm.getRadius() + dirCos*radius + Math.copySign(EXTRA_DISTANCE, dirCos);
		double posY = worm.getPosition().getY() + dirSin*worm.getRadius() + dirSin*radius + Math.copySign(EXTRA_DISTANCE, dirSin);

		return new Projectile(posX, posY, worm.getDirection(), radius, worm.getWorld(), mass, force, hitPointCost);
	}

	/**
	 * Returns the mass of this projectile.
	 * 
	 * @return	the mass of this projectile
	 * 		  |	result == this.mass
	 */
	@Override @Basic
	public double getMass() {
		return this.mass;
	}

	/**
	 * Returns the jump force of this projectile.
	 * 
	 * @return	the jump force of this projectile
	 * 		  |	result == this.force
	 */
	@Override @Basic
	public double getJumpForce() {
		return this.jumpForce;
	}

	/**
	 * Returns the hit point cost of this projectile.
	 * 
	 * @return	the hit point cost of this projectile
	 * 		  |	result == this.hitPointCost
	 */
	@Basic
	public int getHitPointCost() {
		return hitPointCost;
	}

	/**
	 * Returns a Worm this projectile would collide with if it were at the given position, or null
	 * if no such Worm exist.
	 * 
	 * @param 	position
	 *			the position to look for a possible collision with a Worm
	 *
	 * @return	a Worm this projectile would collide with at the given position, or null
	 * 			if no such Worm exists.
	 * 		  |	result == null || ModelUtil.isCollision(position, this.getRadius(), result.getPosition(), result.getRadius()) == true
	 */
	private Worm getHittedWorm(Position position){
		//Loop over all objects in the World
		try{
			for(Worm nextWorm : this.getWorld().getWorms()){
				if(ModelUtil.isCollision(position, this.getRadius(), nextWorm.getPosition(), nextWorm.getRadius())){
					return nextWorm;
				}
			}
		} catch(NullPointerException ex){
			System.out.println(this);
		}

		return null;
	}

	/**
	 * A projectile can always be launched.
	 * 
	 * @param 	timeStepInterval
	 *			this parameter is ignored
	 *
	 * @return	true
	 * 		  |	result == true
	 */
	@Override
	public boolean canJump(double timeStepInterval) {
		return true;
	}

	/**
	 * Returns true if this projectile can be at the given position. A position is jumpable
	 * if this projectile does not collide with impassable terrain or a Worm at the given position. 
	 * 
	 * Used to determine where this projectile will land.
	 * 
	 * @param 	position
	 *			the position to check whether this projectile can be there
	 *
	 * @return	true if the position is a jumpable position: if it is jumpable according to 
	 * 			the definition of the super class and if this projectile does not collide
	 * 			with a Worm at the given position
	 * 		  |	result == super.isJumpablePosition(position) && getHittedWorm(position) == null
	 */
	@Override
	public boolean isJumpablePosition(Position position){
		return super.isJumpablePosition(position) && getHittedWorm(position) == null;
	}

	/**
	 * Position this Projectile at the calculated after-jump location.
	 * 
	 * @param	timeStepInterval
	 * 			The precision the calculations are done with. 
	 * 
	 * @effect	the position of this entity will change to the calculated position. There is a slight difference with the super
	 * 			class's implementation: the projectile will be placed at a position where it collides with impassable terrain or
	 * 			a Worm, and not the position just before this. If this projectile has left the World after a jump, destroy it.
	 *    	  | try
	 *    	  |		this.setPosition(jumpStep(jumpTime(timeStepInterval) + timeStepInterval))
	 *    	  |	catch(IllegalArgumentException)
	 *    	  |		this.destroy()
	 * @post	when a worm was hitted, it's hit points are decreased with this.getHitPointCost(), or if the hit point cost is higher
	 * 			than the current amount of hit ponts of the worm, it's amount of hit points will be zero
	 * 		  |	hittedWorm = getHittedWorm(this.getPosition())
	 * 		  | if(hittedWorm != null)
	 * 		  |		hittedWormNew.getHitPointsCurrent() == hittedWorm.getHitPointsCurrent() - this.getHitPointCost() 
	 * 		  |		|| hittedWormNew.getHitPointsCurrent() == 0
	 * @effect	this projectile will be destroyed
	 * 		  |	this.destroy()
	 */
	@Override
	protected void jump(double timeStepInterval){	
		//We need to add the timeStepInterval, so the projectile eventually collides with the hitted object7
		this.setPosition(jumpStep(jumpTime(timeStepInterval) + timeStepInterval));

		//Now check if we hit something
		Worm hittedWorm = getHittedWorm(this.getPosition());
		if(hittedWorm != null){
			hittedWorm.setHitPointsCurrent(hittedWorm.getHitPointsCurrent() - this.getHitPointCost());
		}

		//Now destroy
		this.destroy();
	}

	/**
	 * Set the jump force of this projectile.
	 * 
	 * @post	the jump force of this projectile is set to the given force
	 * 		  |	new.getJumpForce() == force
	 * 
	 * @throws	IllegalArgumentException
	 * 			The given force is not a valid jump force
	 * 		  | !JumpableEntity.isValidJumpForce(jumpForce)
	 */
	private void setJumpForce(double jumpForce) throws IllegalArgumentException {
		if(!JumpableEntity.isValidJumpForce(jumpForce))
			throw new IllegalArgumentException("The jump force should be postive and a valid Double");
		this.jumpForce = jumpForce;
	}

	/**
	 * Set the mass of this projectile.
	 * 
	 * @post	the mass of this projectile is set to the given mass
	 * 		  |	new.getMass() == mass
	 * 
	 * @throws	IllegalArgumentException
	 * 			The given mass is not a valid mass
	 * 		  | !ModelUtil.isValidMass(mass)
	 */
	private void setMass(double mass) throws IllegalArgumentException{
		if(!ModelUtil.isValidMass(mass))
			throw new IllegalArgumentException("Mass should be postive and a valid Double");
		this.mass = mass;
	}

	/**
	 * Set the hit point cost.
	 * 
	 * @post	the hit point cost of this projectile is set to the given hitPointCost
	 * 		  |	new.getHitPointCost() == hitPointCost
	 */
	private void setHitPointCost(int hitPointCost) {
		this.hitPointCost = hitPointCost;
	}

}
