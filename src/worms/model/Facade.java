package worms.model;

import java.util.Collection;
import java.util.List;
import java.util.Random;

import worms.gui.game.IActionHandler;
import worms.model.programs.*;

/**
 * A class implementing the interface between the GUI and Worm
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class Facade implements IFacade{

	@Override
	public void addEmptyTeam(World world, String newName){
		try{
			world.addTeam(newName);
		} catch (IllegalArgumentException ex) {
			throw new ModelException(ex);
		}

	}

	@Override
	public void addNewFood(World world) {
		world.spawnEntity('f',null);

	}

	@Override
	public void addNewWorm(World world, Program program) {
		world.spawnEntity('w',program);
	}

	@Override
	public boolean canFall(Worm worm) {
		return worm.canFall();
	}

	@Override
	public boolean canMove(Worm worm) {
		return worm.canMove();
	}

	@Override
	public boolean canTurn(Worm worm, double angle) {
		return worm.canTurn(angle);
	}

	@Override
	public Food createFood(World world, double x, double y) {
		Food food = new Food(x,y,world);
		return food;
	}

	@Override
	public World createWorld(double width, double height,
			boolean[][] passableMap, Random random) {
		return new World(width,height,passableMap,random);
	}

	@Override
	public Worm createWorm(World world, double x, double y, double direction, double radius, String name, Program program) {
		try{
			Worm worm = new Worm(x, y, direction, radius, name,world,program);
			return worm;
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex);
		}
	}

	@Override
	public void fall(Worm worm){
		try{
			worm.fall();
		} catch(IllegalMovementException ex){
			throw new ModelException(ex);
		} catch(IllegalPositionException ex){
			System.out.println("fall: "+ex.getMessage());
		}

	}

	@Override
	public int getActionPoints(Worm worm) {
		return worm.getActionPointsCurrent();
	}

	@Override
	public Projectile getActiveProjectile(World world) {
		return world.getActiveProjectile();
	}

	@Override
	public Worm getCurrentWorm(World world) {
		return world.getActiveWorm();
	}

	@Override
	public Collection<Food> getFood(World world) {
		return world.getFoods();
	}

	@Override
	public int getHitPoints(Worm worm) {
		return worm.getHitPointsCurrent();
	}

	@Override
	public double[] getJumpStep(Projectile projectile, double t) {
		try{
			Position p = projectile.jumpStep(t);
			double[] position = {p.getX(), p.getY()};

			return position;
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex);
		}		
	}

	@Override
	public double[] getJumpStep(Worm worm, double t) {
		try{
			Position p = worm.jumpStep(t);
			double[] position = {p.getX(), p.getY()};

			return position;
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex);
		}
	}

	@Override
	public double getJumpTime(Projectile projectile, double timeStep) {
		try{
			return projectile.jumpTime(timeStep);
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex);
		}
	}

	@Override
	public double getJumpTime(Worm worm, double timeStep) {
		try{
			return worm.jumpTime(timeStep);
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex);
		}
	}

	@Override
	public double getMass(Worm worm) {
		return worm.getMass();
	}

	@Override
	public int getMaxActionPoints(Worm worm) {
		return worm.getActionPointsMax();
	}

	@Override
	public int getMaxHitPoints(Worm worm) {
		return worm.getHitPointsMax();
	}

	@Override
	public double getMinimalRadius(Worm worm) {
		return Worm.getRadiusLowerBound();
	}

	@Override
	public String getName(Worm worm) {
		return worm.getName();
	}

	@Override
	public double getOrientation(Worm worm) {
		return worm.getDirection();
	}

	@Override
	public double getRadius(Food food) {
		return food.getRadius();
	}

	@Override
	public double getRadius(Projectile projectile) {
		return projectile.getRadius();
	}

	@Override
	public double getRadius(Worm worm) {
		return worm.getRadius();
	}

	@Override
	public String getSelectedWeapon(Worm worm) {
		return worm.getSelectedWeapon().getName();

	}

	@Override
	public String getTeamName(Worm worm){
		if(worm.getTeam() == null){
			throw new ModelException("This worm has no team");
		} else{
			return worm.getTeam().getName();
		}
	}

	@Override
	public String getWinner(World world) {
		return world.getWinner();
	}

	@Override
	public Collection<Worm> getWorms(World world) {
		return world.getWorms();
	}

	@Override
	public double getX(Food food) {
		return food.getPosition().getX();
	}

	@Override
	public double getX(Projectile projectile) {
		return projectile.getPosition().getX();
	}

	@Override
	public double getX(Worm worm) {
		return worm.getPosition().getX();
	}

	@Override
	public double getY(Food food) {
		return food.getPosition().getY();
	}


	@Override
	public double getY(Projectile projectile) {
		return projectile.getPosition().getY();
	}


	@Override
	public double getY(Worm worm) {
		return worm.getPosition().getY();
	}

	@Override
	public boolean isActive(Food food) {
		return food.isActive();
	}

	@Override
	public boolean isActive(Projectile projectile) {
		return projectile.isActive();
	}

	@Override
	public boolean isAdjacent(World world, double x, double y, double radius) {
		Position p = new Position(x,y);
		return world.isAdjacent(p, radius);
	}

	@Override
	public boolean isAlive(Worm worm) {
		return worm.isActive();
	}

	@Override
	public boolean isGameFinished(World world) {
		return world.isGameFinished();
	}

	@Override
	public boolean isImpassable(World world, double x, double y, double radius) {
		Position p = new Position(x,y);
		return !world.isPassable(p, radius);
	}

	@Override
	public void jump(Projectile projectile, double timeStep){
		try{
			projectile.jump(timeStep);
		}catch(IllegalPositionException ex){
			//happens when you shoot outside the map
			System.out.println("jump projectile: "+ex.getMessage());
		}
	}

	@Override
	public void jump(Worm worm, double timeStep){
		try{
			worm.jump(timeStep);
		} catch (IllegalMovementException ex){
			throw new ModelException(ex);
		} catch (IllegalPositionException ex){
			System.out.println("jump worm: "+ex.getMessage());
		}
	}

	@Override
	public void move(Worm worm){
		try{
			worm.move();
		} catch (IllegalMovementException ex){
			throw new ModelException(ex);
		} catch (IllegalPositionException ex){
			System.out.println("move: "+ex.getMessage());
		}
	}

	@Override
	public void rename(Worm worm, String newName) {
		try{
			worm.setName(newName);
		} catch (IllegalArgumentException ex){
			throw new ModelException(ex);
		}			
	}

	@Override
	public void selectNextWeapon(Worm worm) {
		worm.selectNextWeapon();		
	}

	@Override
	public void setRadius(Worm worm, double newRadius) {
		try{
			worm.setRadius(newRadius);		
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex);
		}
	}

	@Override
	public void shoot(Worm worm, int yield) {
		try{
			worm.shoot(yield);
		} catch (IllegalShootException ex){
			throw new ModelException(ex);
		} catch(IllegalArgumentException ex){
			throw new ModelException(ex);
		}
	}

	@Override
	public void startGame(World world) {
		if(world.startGame());
			this.startNextTurn(world);
	}

	@Override
	public void startNextTurn(World world) {
		do {
			try{
				world.nextTurn();
			}catch(IllegalStateException ex){
				System.out.println(ex.getMessage());
			}
		}while(world.getActiveWorm().getProgram() != null);
	}

	@Override
	public void turn(Worm worm, double angle) {
		if(!worm.canTurn(angle))
			throw new ModelException("The given angle is not valid for turning");

		worm.turn(angle);		
	}

	@Override
	public ParseOutcome<?> parseProgram(String programText, IActionHandler handler) {
		ProgramParser<Expression<?>,Statement,BNFObject> parser = new ProgramParser<Expression<?>,Statement,BNFObject>(new ProgramPlantation());
		try{
			parser.parse(programText);
		}catch (IllegalArgumentException ex){
			throw new ModelException(ex);
		}
		List<String> errors = parser.getErrors();
		if(errors.isEmpty()){
			Program program = new Program(parser.getGlobals(), parser.getStatement(), handler);
			return ParseOutcome.success(program);
		}else{
			return ParseOutcome.failure(errors);
		}

	}

	@Override
	public boolean hasProgram(Worm worm) {
		return(worm.getProgram() != null);
	}

	@Override
	public boolean isWellFormed(Program program) {
		return program.isWellFormed();
	}
}
