package worms.model;


/**
 * A class of utility functions
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class ModelUtil {

	/**
	 * Get the distance between two positions
	 * 
	 * @param positionA
	 * @param positionB
	 * 
	 * @return the distance
	 * 		|	result == Math.sqrt(Math.pow((positionA.getX() - positionB.getX()),2) + Math.pow((positionA.getY() - positionB.getY()),2))
	 */
	public static double distanceBetweenPositions(Position positionA, Position positionB){
		return Math.sqrt(Math.pow((positionA.getX() - positionB.getX()),2) + Math.pow((positionA.getY() - positionB.getY()),2));
	}
	
	/**
	 * Check whether two entities are colliding
	 * 
	 * @param a	an entity
	 * @param b	another entity
	 * 
	 * @return	whether a and b are colliding
	 * 		| result == ModelUtil.isCollision(a.getPosition(), a.getRadius(), b.getPosition(), b.getRadius())
	 */
	public static boolean isCollision(Entity a, Entity b){
		return ModelUtil.isCollision(a.getPosition(), a.getRadius(), b.getPosition(), b.getRadius());
	}
	
	/**
	 * Check whether the given radiuses and positions result in a collision
	 * 
	 * @param pa first position
	 * @param ra first radius
	 * @param pb second position
	 * @param rb second radius
	 * 
	 * @pre	the radiuses must be valid
	 * 		| assert(ra >= 0 && rb >= 0)
	 * @return whether they are colliding
	 * 		| result == distanceBetweenPositions(pa, pb) < (ra + rb)
	 * 
	 */
	public static boolean isCollision(Position pa, double ra, Position pb, double rb){
		assert(ra >= 0 && rb >= 0);
		
		return distanceBetweenPositions(pa, pb) < (ra + rb);
	}
	
	/**
	 * Check whether a given mass is valid
	 * 
	 * @param mass
	 * 
	 * @return is it a valid mass
	 * 		|	result == !(mass < 0 || Double.isNaN(mass))
	 */
	public static boolean isValidMass(double mass){
		return !(mass < 0 || Double.isNaN(mass));
	}
	
	/**
	 * Return whether a given double is valid for using in World
	 * 
	 * @param a the double to check
	 * 
	 * @return whether it is valid
	 * 		| result == !(Double.isNaN(a) || Double.isInfinite(a))
	 */
	public static boolean isValidNumber(double a){
		return !(Double.isNaN(a) || Double.isInfinite(a));
	}
}
