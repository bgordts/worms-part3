package worms.model;


/**
 * A class to represent a food entity
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Food extends Entity{
	
	private static final double radius = 0.2;
	
	/**
	 * Construct a food entity with radius 0.2
	 * @param x	the x-coordinate
	 * @param y	the y-coordinate
	 * @param world the containing world
	 * 
	 * @effect make an entity with x, y, and world
	 * 		| super(x,y, -1, world)
	 */
	protected Food(double x, double y, World world) {
		super(x,y, 1, world);
	}
	
	/**
	 * Food's radius is set
	 * 
	 * @return the radius
	 * 		|	result == Food.radius
	 */
	public double getRadius(){
		return Food.radius;
	}
	
	/**
	 * Food's radius is set
	 * 
	 * @return the radius
	 * 		|	result == Food.radius
	 */
	public static double getFoodRadius(){
		return Food.radius;
	}

	/**
	 * Food's radius cannot be changed
	 */
	protected void setRadius(double a){
		return;
	}

}
