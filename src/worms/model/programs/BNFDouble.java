package worms.model.programs;

/**
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class BNFDouble extends BNFObject {

	protected BNFDouble(){
		super(0.0);
	}
	
	protected BNFDouble(Double value) {
		super(value);
	}

	public Double get(){
		return (Double) super.get();
	}

	@Override
	protected void set(Object value) throws IllegalArgumentException{
		if(!(value instanceof Double))
			throw new IllegalArgumentException("Argument must be of type Double");
		super.setObject(value);
		
	}

}
