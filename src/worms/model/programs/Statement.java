package worms.model.programs;

import java.util.List;

import worms.model.Food;
import worms.model.Program;
import worms.model.Worm;
import worms.model.programs.ProgramFactory.ForeachType;

/**
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class Statement {

	public enum StatementType{
		TURN,MOVE,JUMP,TOGGLE,FIRE,SKIP,
		ASSIGN,IF,WHILE,FOREACH,SEQUENCE,PRINT
	}

	private final StatementType type;
	private Expression<?> argument;
	private Statement body;
	private Statement otherwise;
	private String variableName;
	private ForeachType foreachType;
	private List<Statement> sequence;
	
	private int resume = -1;
	
	public static class Factory{
		
		public static Statement createTurn(int line, int column, Expression<BNFDouble> angle) {
			return new Statement(line, column, StatementType.TURN,angle);
		}

		public static Statement createMove(int line, int column) {
			return new Statement(line, column, StatementType.MOVE);
		}

		public static Statement createJump(int line, int column) {
			return new Statement(line, column, StatementType.JUMP);
		}

		public static Statement createToggleWeap(int line, int column) {
			return new Statement(line, column, StatementType.TOGGLE);
		}

		public static Statement createFire(int line, int column, Expression<BNFDouble> yield) {
			return new Statement(line, column, StatementType.FIRE,yield);
		}

		public static Statement createSkip(int line, int column) {
			return new Statement(line, column, StatementType.SKIP);
		}

		public static Statement createAssignment(int line, int column, String variableName, Expression<?> rhs) {
			return new Statement(line, column, StatementType.ASSIGN,variableName,rhs);
		}

		public static Statement createIf(int line, int column, Expression<BNFBoolean> condition, Statement then, Statement otherwise) {
			return new Statement(line, column, StatementType.IF,condition,then,otherwise);
		}

		public static Statement createWhile(int line, int column, Expression<BNFBoolean> condition, Statement body) {
			return new Statement(line, column, StatementType.WHILE,condition,body);
		}

		public static Statement createForeach(int line, int column, ForeachType type, String variableName, Statement body) {
			return new Statement(line, column, StatementType.FOREACH,type,variableName,body);
		}

		public static Statement createSequence(int line, int column, List<Statement> statements) {
			return new Statement(line, column, StatementType.SEQUENCE,statements);
		}

		public static Statement createPrint(int line, int column, Expression<?> e) {
			return new Statement(line, column, StatementType.PRINT,e);
		}
	}

	/**
	 * Constructor for statements of type MOVE,JUMP,TOGGLE,SKIP
	 * @param type the type of statement
	 */
	private Statement(int line, int column, StatementType type){
		this(line,column,type,null,null,null,null,null,null);

	}

	/**
	 * Constructor for statements of type TURN,FIRE,PRINT
	 * @param type
	 * @param argument
	 */
	private Statement(int line, int column, StatementType type, Expression<?> argument) {
		this(line,column,type,argument,null,null,null,null,null);
	}

	/**
	 * Constructor for statements of type WHILE
	 * @param type
	 * @param condition
	 * @param body
	 */
	private Statement(int line, int column, StatementType type, Expression<BNFBoolean> condition, Statement body){
		this(line,column,type,condition,body,null,null,null,null);
	}
	
	/**
	 * Constructor for statements of type IF
	 * @param type
	 * @param condition
	 * @param then
	 * @param otherwise
	 */
	private Statement(int line, int column, StatementType type, Expression<BNFBoolean> condition, Statement then, Statement otherwise){
		this(line,column,type,condition,then,otherwise,null,null,null);
	}

	/**
	 * Constructor for statements of type ASSIGN
	 * @param type
	 * @param variableName
	 * @param rhs
	 */
	private Statement(int line, int column, StatementType type, String variableName, Expression<?> rhs){
		this(line,column,type,rhs,null,null,variableName,null,null);
	}

	/**
	 * Constructor for statements of type FOREACH
	 * @param type
	 * @param foreachType
	 */
	private Statement(int line, int column, StatementType type, ForeachType foreachType, String variableName, Statement body){
		this(line,column,type,null,body,null,variableName,foreachType,null);
	}

	/**
	 * Constructor for statements of type SEQUENCE
	 * @param type
	 * @param statements
	 */
	private Statement(int line, int column, StatementType type, List<Statement> statements) {
		this(line,column,type,null,null,null,null,null,statements);
	}

	private Statement(int line, int column, StatementType type, Expression<?> argument, Statement body, Statement otherwise, String variableName, ForeachType foreachType, List<Statement> sequence){
		this.type = type;
		this.argument = argument;
		this.body = body;
		this.otherwise = otherwise;
		this.variableName = variableName;
		this.foreachType = foreachType;
		this.sequence = sequence;
		check(line,column);
	}

	private void check(int line, int column) throws IllegalArgumentException{
		String error = "Compilation error at line "+line+" column "+column+":Statement "+this.type;
		switch(this.type){
		case MOVE:
		case JUMP:
		case TOGGLE:
		case SKIP:
			break;
		case TURN:
		case FIRE:
			if(argument.getType().cls() != Double.class)
				throw new IllegalArgumentException(error+" expects 1 expression with return type double");
			break;
		case IF:
		case WHILE:
			if(argument.getType().cls() != Boolean.class)
				throw new IllegalArgumentException(error+" expects 1 expression with return type boolean");
			break;
		case FOREACH:
			if(variableName == null || body == null)
				throw new IllegalArgumentException(error+" expects non-null arguments");
			break;
		case ASSIGN:
			if(variableName == null || argument == null)
				throw new IllegalArgumentException(error+" expects non-null arguments");
			break;
		case SEQUENCE:
			if(sequence == null)
				throw new IllegalArgumentException(error+" expects non-null arguments");
			break;
		case PRINT:
			if(argument == null)
				throw new IllegalArgumentException(error+" expects non-null arguments");
			break;
		}
	}

	public void execute(Program program){
		program.push(this);
		try{ switch(this.getType()){
		case TURN:
			Thread.sleep(100);
			BNFDouble argument = (BNFDouble) this.argument.execute(program);
			if(!program.getWorm().hasEnoughActionPointsToTurn(argument.get())){
				throw new StatementInterruptException("Not enough action points to turn");
			} else{
				program.getHandler().turn(program.getWorm(), ((BNFDouble) this.argument.execute(program)).get());
			}
			break;
		case MOVE:
			Thread.sleep(100);
			if(!program.getWorm().hasEnoughActionPointsToMove()){
				throw new StatementInterruptException("Not enough action points to move");
			} else {
				program.getHandler().move(program.getWorm());
			}
			break;
		case JUMP:
			Thread.sleep(100);
			if(!program.getWorm().hasEnoughActionPointsToJump()){
				throw new StatementInterruptException("Not enough action points to jump");
			} else {
				program.getHandler().jump(program.getWorm());
			}
			break;
		case TOGGLE:
			Thread.sleep(100);
			program.getHandler().toggleWeapon(program.getWorm());
			break;
		case FIRE:
			Thread.sleep(100);
			if(!program.getWorm().hasEnoughActionPointsToShoot()){
				throw new StatementInterruptException("Not enough action points to fire");
			} else {
				program.getHandler().fire(program.getWorm(), ((BNFDouble) this.argument.execute(program)).get().intValue());
			}
			break;
		case SKIP:
			Thread.sleep(1000);
			break;
		case ASSIGN:
			program.getGlobals().get(this.variableName).set(this.argument.execute(program).get());

			break;
		case IF:	
			this.ifStatement(program);
			break;
		case WHILE:	
			this.whileStatement(program);
			break;
		case FOREACH:
			this.foreachStatement(program);
			break;
		case SEQUENCE:
			this.sequenceStatement(program);
			break;
		case PRINT:	
			System.out.println(this.argument.execute(program).get());
			break;
		} } catch(InterruptedException ex){}
		
		program.pop();
	}

	private void ifStatement(Program program) {
		if(resume == -1){
			if((boolean) this.argument.execute(program).get()){
				resume = 0;
				this.body.execute(program);
				resume = -1;
			}else{
				resume = 0;
				this.otherwise.execute(program);
				resume = -1;
			}
		}
		resume = -1;
	}

	private void whileStatement(Program program) {
		while((boolean) this.argument.execute(program).get())
			this.body.execute(program);

	}

	private void foreachStatement(Program program) {
		BNFEntity ent = (BNFEntity) program.getGlobals().get(this.variableName);
		if(this.foreachType == ForeachType.FOOD || this.foreachType == ForeachType.ANY){
			Iterable<Food> foods = program.getWorm().getWorld().getFoods();
			for(Food food: foods){
				ent.set(food);
				this.body.execute(program);
			}
		}

		if(this.foreachType == ForeachType.WORM || this.foreachType == ForeachType.ANY){
			Iterable<Worm> worms = program.getWorm().getWorld().getWorms();
			for(Worm worm: worms){
				ent.set(worm);
				this.body.execute(program);
			}
		}

	}

	private void sequenceStatement(Program program){
		resume++;
		for(; resume < this.sequence.size(); resume++){
			this.sequence.get(resume).execute(program);
		}
		resume = -1;
	}

	private StatementType getType(){
		return this.type;
	}

	public boolean isWellFormed(boolean inForEach) {
		switch(this.type){
		case TURN:
		case JUMP:
		case MOVE:
		case TOGGLE:
		case FIRE:
		case SKIP:
			if(inForEach)
				return false;
			else 
				return true;
		case ASSIGN:
		case PRINT:
			return true;
		case IF:
			return body.isWellFormed(inForEach) && otherwise.isWellFormed(inForEach);
		case WHILE:
			return body.isWellFormed(inForEach);
		case FOREACH:
			return body.isWellFormed(true);
		case SEQUENCE:
			boolean result = true;
			for(Statement s: sequence)
				result &= s.isWellFormed(inForEach);
			return result;
		default:
			assert(false);
			return false;
		}
	}

}
