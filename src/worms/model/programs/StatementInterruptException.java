package worms.model.programs;

/**
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class StatementInterruptException extends RuntimeException {
	
	public StatementInterruptException(String message){
		super(message);
	}
	private static final long serialVersionUID = 5097555792974353080L;

}
