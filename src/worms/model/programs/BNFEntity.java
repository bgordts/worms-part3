package worms.model.programs;

import worms.model.Entity;

/**
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class BNFEntity extends BNFObject {
	
	protected BNFEntity(){
		super(null);
	}

	protected BNFEntity(Entity value) {
		super(value);
	}
	
	public Entity get(){
		return (Entity) super.get();
	}

	@Override
	protected void set(Object value) throws IllegalArgumentException{
		if(!(value instanceof Entity || value == null))
			throw new IllegalArgumentException("Argument must be of type Entity");
		super.setObject(value);
		
	}

}
