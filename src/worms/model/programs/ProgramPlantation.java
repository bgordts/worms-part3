package worms.model.programs;

import java.util.List;

/**
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
@SuppressWarnings("unchecked")
public class ProgramPlantation implements ProgramFactory<Expression<?>, Statement, BNFObject> {
	@Override
	public Expression<BNFDouble> createDoubleLiteral(int line, int column, double d) {
		return Expression.Factory.createDoubleLiteral(line, column, d);
	}

	@Override
	public Expression<BNFBoolean> createBooleanLiteral(int line, int column, boolean b) {
		return Expression.Factory.createBooleanLiteral(line, column, b);
	}

	public Expression<BNFBoolean> createAnd(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createAnd(line, column, (Expression<BNFBoolean>) e1, (Expression<BNFBoolean>) e2);
	}

	@Override
	public Expression<BNFBoolean> createOr(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createOr(line, column, (Expression<BNFBoolean>) e1, (Expression<BNFBoolean>) e2);
	}

	@Override
	public Expression<BNFBoolean> createNot(int line, int column, Expression<?> e) {
		return Expression.Factory.createNot(line, column, (Expression<BNFBoolean>) e);
	}

	@Override
	public Expression<?> createNull(int line, int column) {
		return Expression.Factory.createNull(line, column);
	}

	@Override
	public Expression<BNFEntity> createSelf(int line, int column) {
		return Expression.Factory.createSelf(line, column);
	}

	@Override
	public Expression<BNFDouble> createGetX(int line, int column, Expression<?> e) {
		return Expression.Factory.createGetx(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFDouble> createGetY(int line, int column, Expression<?> e) {
		return Expression.Factory.createGety(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFDouble> createGetRadius(int line, int column, Expression<?> e) {
		return Expression.Factory.createGetRadius(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFDouble> createGetDir(int line, int column, Expression<?> e) {
		return Expression.Factory.createGetDir(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFDouble> createGetAP(int line, int column, Expression<?> e) {
		return Expression.Factory.createGetAP(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFDouble> createGetMaxAP(int line, int column, Expression<?> e) {
		return Expression.Factory.createGetMaxAP(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFDouble> createGetHP(int line, int column, Expression<?> e) {
		return Expression.Factory.createGetHP(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFDouble> createGetMaxHP(int line, int column, Expression<?> e) {
		return Expression.Factory.createGetMaxHP(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFBoolean> createSameTeam(int line, int column, Expression<?> e) {
		return Expression.Factory.createSameTeam(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFEntity> createSearchObj(int line, int column, Expression<?> e) {
		return Expression.Factory.createSearchObj(line, column, (Expression<BNFDouble>) e);
	}

	@Override
	public Expression<BNFBoolean> createIsWorm(int line, int column, Expression<?> e) {
		return Expression.Factory.createIsWorm(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFBoolean> createIsFood(int line, int column, Expression<?> e) {
		return Expression.Factory.createIsFood(line, column, (Expression<BNFEntity>) e);
	}

	@Override
	public Expression<BNFBoolean> createVariableAccess(int line, int column, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Expression<?> createVariableAccess(int line, int column, String name, BNFObject type) throws IllegalArgumentException{
		if(type instanceof BNFBoolean)
			return Expression.Factory.createGetBoolean(line, column, name);
		else if(type instanceof BNFDouble)
			return Expression.Factory.createGetDouble(line, column, name);
		else if(type instanceof BNFEntity)
			return Expression.Factory.createGetEntity(line, column, name);

		throw new IllegalArgumentException("Compilation error line "+line+" column "+column+": Unknown BNFObject type");

	}

	@Override
	public Expression<BNFBoolean> createLessThan(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createLessThan(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFBoolean> createGreaterThan(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createGreaterThan(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFBoolean> createLessThanOrEqualTo(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createLessThanOrEqualTo(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFBoolean> createGreaterThanOrEqualTo(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createGreaterThanOrEqualTo(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFBoolean> createEquality(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createEquality(line, column, e1, e2);
	}

	@Override
	public Expression<BNFBoolean> createInequality(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createInequality(line, column, e1, e2);
	}

	@Override
	public Expression<BNFDouble> createAdd(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createAdd(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFDouble> createSubtraction(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createSubtraction(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFDouble> createMul(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createMul(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFDouble> createDivision(int line, int column, Expression<?> e1, Expression<?> e2) {
		return Expression.Factory.createDivision(line, column, (Expression<BNFDouble>) e1, (Expression<BNFDouble>) e2);
	}

	@Override
	public Expression<BNFDouble> createSqrt(int line, int column, Expression<?> e) {
		return Expression.Factory.createSqrt(line, column, (Expression<BNFDouble>) e);
	}

	@Override
	public Expression<BNFDouble> createSin(int line, int column, Expression<?> e) {
		return Expression.Factory.createSin(line, column, (Expression<BNFDouble>) e);
	}

	@Override
	public Expression<BNFDouble> createCos(int line, int column, Expression<?> e) {
		return Expression.Factory.createCos(line, column, (Expression<BNFDouble>) e);
	}

	@Override
	public Statement createTurn(int line, int column, Expression<?> angle) {
		return Statement.Factory.createTurn(line, column, (Expression<BNFDouble>) angle);
	}

	@Override
	public Statement createMove(int line, int column) {
		return Statement.Factory.createMove(line, column);
	}

	@Override
	public Statement createJump(int line, int column) {
		return Statement.Factory.createJump(line, column);
	}

	@Override
	public Statement createToggleWeap(int line, int column) {
		return Statement.Factory.createToggleWeap(line, column);
	}

	@Override
	public Statement createFire(int line, int column, Expression<?> yield) {
		return Statement.Factory.createFire(line, column, (Expression<BNFDouble>) yield);
	}

	@Override
	public Statement createSkip(int line, int column) {
		return Statement.Factory.createSkip(line, column);
	}

	@Override
	public Statement createAssignment(int line, int column, String variableName, Expression<?> rhs) {
		return Statement.Factory.createAssignment(line, column, variableName, rhs);
	}

	@Override
	public Statement createIf(int line, int column, Expression<?> condition, Statement then, Statement otherwise) {
		return Statement.Factory.createIf(line, column, (Expression<BNFBoolean>) condition, then, otherwise);
	}

	@Override
	public Statement createWhile(int line, int column, Expression<?> condition, Statement body) {
		return Statement.Factory.createWhile(line, column, (Expression<BNFBoolean>) condition, body);
	}

	@Override
	public Statement createForeach(int line, int column, ForeachType type, String variableName, Statement body) {
		return Statement.Factory.createForeach(line, column, type, variableName, body);
	}

	@Override
	public Statement createSequence(int line, int column, List<Statement> statements) {
		return Statement.Factory.createSequence(line, column, statements);
	}

	@Override
	public Statement createPrint(int line, int column, Expression<?> e) {
		return Statement.Factory.createPrint(line, column, e);
	}

	@Override
	public BNFObject createDoubleType() {
		return new BNFDouble();
	}

	@Override
	public BNFObject createBooleanType() {
		return new BNFBoolean();
	}

	@Override
	public BNFObject createEntityType() {
		return new BNFEntity();
	}

}
