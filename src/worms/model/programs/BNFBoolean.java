package worms.model.programs;

/**
 * 
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class BNFBoolean extends BNFObject {
	

	protected BNFBoolean(){
		super(false);
	}
	
	protected BNFBoolean(Boolean value) {
		super(value);
	}
	
	public Boolean get(){
		return (Boolean) super.get();
	}

	@Override
	protected void set(Object value) throws IllegalArgumentException{
		if(!(value instanceof Boolean))
			throw new IllegalArgumentException("Argument must be of type Boolean");
		super.setObject(value);
		
	}

}
