package worms.model.programs;

/**
 * Base class where every supported type in the BNF language inherits from. 
 *
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */

public abstract class BNFObject {
	
	private Object value;
	
	protected BNFObject(Object value) {
		this.set(value);
	}
	
	protected abstract void set(Object value);
	
	protected void setObject(Object value){
		this.value = value;
	}
	
	public Object get(){
		return this.value;
	}
}
