package worms.model.programs;

import worms.model.Entity;
import worms.model.Food;
import worms.model.Program;
import worms.model.Team;
import worms.model.Worm;

/**
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Expression<T extends BNFObject>{
	
	public enum ExpressionType {
		BOOLEAN(Boolean.class),	DOUBLE(Double.class),		
		AND(Boolean.class),		OR(Boolean.class),			NOT(Boolean.class),			NULL(Entity.class),
		SELF(Entity.class),		GETX(Double.class), 		GETY(Double.class), 		GETRADIUS(Double.class),
		GETDIR(Double.class),	GETAP(Double.class), 		GETMAXAP(Double.class), 	GETHP(Double.class), 			
		GETMAXHP(Double.class),	SAMETEAM(Boolean.class),	SEARCH(Entity.class),		ISWORM(Boolean.class), 	
		ISFOOD(Boolean.class),	GETENTITY(Entity.class),	GETBOOLEAN(Boolean.class),	GETDOUBLE(Double.class),
		LESS(Boolean.class),	LESSOREQUAL(Boolean.class), GREATER(Boolean.class), 	GREATEROREQUAL(Boolean.class), 	
		EQUAL(Boolean.class),	ADD(Double.class),			SUBTRACT(Double.class),		MULTIPLY(Double.class),	
		DIVISION(Double.class),	SQRT(Double.class), 		SIN(Double.class), 			COS(Double.class);	
		
		private final Class<?> cls;
		private ExpressionType(Class<?> cls){
			this.cls = cls;
		}
		public Class<?> cls(){return cls;}
	}
	
	private final ExpressionType type;
	
	private Double d;
	private Boolean b;
	private String variable;
	private Expression<?> e1;
	private Expression<?> e2;
	
	protected static class Factory{
		
		protected static Expression<BNFDouble> createDoubleLiteral(int line, int column, double d) {
			return new Expression<BNFDouble>(line, column, ExpressionType.DOUBLE,d);
		}

		protected static Expression<BNFBoolean> createBooleanLiteral(int line, int column, boolean b) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.BOOLEAN,b);
		}
		
		protected static Expression<BNFBoolean> createAnd(int line, int column, Expression<BNFBoolean> e1, Expression<BNFBoolean> e2){
			return new Expression<BNFBoolean>(line, column, ExpressionType.AND, e1, e2);
		}
		
		protected static Expression<BNFBoolean> createOr(int line, int column, Expression<BNFBoolean> e1, Expression<BNFBoolean> e2){
			return new Expression<BNFBoolean>(line, column, ExpressionType.OR, e1, e2);
		}
		
		protected static Expression<BNFBoolean> createNot(int line, int column, Expression<BNFBoolean> e1){
			return new Expression<BNFBoolean>(line, column, ExpressionType.NOT, e1);
		}
		
		protected static Expression<BNFEntity> createNull(int line, int column){
			return new Expression<BNFEntity>(line, column, ExpressionType.NULL);
		}
		
		protected static Expression<BNFEntity> createSelf(int line, int column){
			return new Expression<BNFEntity>(line, column, ExpressionType.SELF);
		}
		
		protected static Expression<BNFDouble> createGetx(int line, int column, Expression<BNFEntity> e1){
			return new Expression<BNFDouble>(line, column, ExpressionType.GETX, e1);
		}
		
		protected static Expression<BNFDouble> createGety(int line, int column, Expression<BNFEntity> e1){
			return new Expression<BNFDouble>(line, column, ExpressionType.GETY, e1);
		}
		
		protected static Expression<BNFDouble> createGetRadius(int line, int column, Expression<BNFEntity> e1){
			return new Expression<BNFDouble>(line, column, ExpressionType.GETRADIUS, e1);
		}
		
		protected static Expression<BNFDouble> createGetDir(int line, int column, Expression<BNFEntity> e1){
			return new Expression<BNFDouble>(line, column, ExpressionType.GETDIR, e1);
		}
		
		protected static Expression<BNFDouble> createGetAP(int line, int column, Expression<BNFEntity> e1){
			return new Expression<BNFDouble>(line, column, ExpressionType.GETAP, e1);
		}
		
		protected static Expression<BNFDouble> createGetMaxAP(int line, int column, Expression<BNFEntity> e1){
			return new Expression<BNFDouble>(line, column, ExpressionType.GETMAXAP);
		}
		
		protected static Expression<BNFDouble> createGetHP(int line, int column, Expression<BNFEntity> e) {
			return new  Expression<BNFDouble>(line, column, ExpressionType.GETHP,e);
		}

		protected static Expression<BNFDouble> createGetMaxHP(int line, int column, Expression<BNFEntity> e) {
			return new Expression<BNFDouble>(line, column, ExpressionType.GETMAXHP,e);
		}

		protected static Expression<BNFBoolean> createSameTeam(int line, int column, Expression<BNFEntity> e) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.SAMETEAM,e);
		}

		protected static Expression<BNFEntity> createSearchObj(int line, int column, Expression<BNFDouble> e) {
			return new Expression<BNFEntity>(line, column, ExpressionType.SEARCH,e);
		}

		protected static Expression<BNFBoolean> createIsWorm(int line, int column, Expression<BNFEntity> e) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.ISWORM,e);
		}

		protected static Expression<BNFBoolean> createIsFood(int line, int column, Expression<BNFEntity> e) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.ISFOOD,e);
		}
		
		protected static Expression<BNFBoolean> createGetBoolean(int line, int column, String name) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.GETBOOLEAN,name);
		}
		
		protected static Expression<BNFDouble> createGetDouble(int line, int column, String name) {
			return new Expression<BNFDouble>(line, column, ExpressionType.GETDOUBLE,name);
		}
		
		protected static Expression<BNFEntity> createGetEntity(int line, int column, String name) {
			return new Expression<BNFEntity>(line, column, ExpressionType.GETENTITY,name);
		}
		
		protected static Expression<BNFBoolean> createLessThan(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.LESS,e1,e2);
		}

		protected static Expression<BNFBoolean> createGreaterThan(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.GREATER,e1,e2);
		}

		protected static Expression<BNFBoolean> createLessThanOrEqualTo(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.LESSOREQUAL,e1,e2);
		}

		protected static Expression<BNFBoolean> createGreaterThanOrEqualTo(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.GREATEROREQUAL,e1,e2);
		}

		protected static Expression<BNFBoolean> createEquality(int line, int column, Expression<?> e1, Expression<?> e2) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.EQUAL,e1,e2);
		}

		protected static Expression<BNFBoolean> createInequality(int line, int column, Expression<?> e1, Expression<?> e2) {
			return new Expression<BNFBoolean>(line, column, ExpressionType.NOT,new Expression<BNFBoolean>(line, column, ExpressionType.EQUAL,e1,e2));
		}

		protected static Expression<BNFDouble> createAdd(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFDouble>(line, column, ExpressionType.ADD,e1,e2);
		}

		protected static Expression<BNFDouble> createSubtraction(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFDouble>(line, column, ExpressionType.SUBTRACT,e1,e2);
		}

		protected static Expression<BNFDouble> createMul(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFDouble>(line, column, ExpressionType.MULTIPLY,e1,e2);
		}

		protected static Expression<BNFDouble> createDivision(int line, int column, Expression<BNFDouble> e1, Expression<BNFDouble> e2) {
			return new Expression<BNFDouble>(line, column, ExpressionType.DIVISION,e1,e2);
		}

		protected static Expression<BNFDouble> createSqrt(int line, int column, Expression<BNFDouble> e) {
			return new Expression<BNFDouble>(line, column, ExpressionType.SQRT,e);
		}

		protected static Expression<BNFDouble> createSin(int line, int column, Expression<BNFDouble> e) {
			return new Expression<BNFDouble>(line, column, ExpressionType.SIN,e);
		}

		protected static Expression<BNFDouble> createCos(int line, int column, Expression<BNFDouble> e) {
			return new Expression<BNFDouble>(line, column, ExpressionType.COS,e);
		}
		
	}
	
	private Expression(int line, int column, ExpressionType type){
		this(line,column,type,null,	null,	null,	null,	null);
	}
	
	private Expression(int line, int column, ExpressionType type, Boolean b){
		this(line,column,type,b,	null,	null,	null,	null);
	}
	
	private Expression(int line, int column, ExpressionType type, Double d){
		this(line,column,type,null,	d,		null,	null,	null);
	}
	
	private Expression(int line, int column, ExpressionType type, String variable){
		this(line,column,type,null,	null,	variable,null,	null);
	}
	
	private Expression(int line, int column, ExpressionType type, Expression<?> e){
		this(line,column,type,null,	null,	null,	e,		null);
	}
	
	private Expression(int line, int column, ExpressionType type, Expression<?> e1, Expression<?> e2){
		this(line,column,type,null,	null,	null,	e1,		e2);
	}
	
	private Expression(int line, int column, ExpressionType type, Boolean b, Double d, String variable, Expression<?> e1, Expression<?> e2){
		this.type = type;
		this.b = b;
		this.d = d;
		this.variable = variable;
		this.e1 = e1;
		this.e2 = e2;
		check(line, column);
	}
	
	private void check(int line, int column) throws IllegalArgumentException{
		String error = "Compilation error at line "+line+" column "+column+":Expression "+this.type;
		switch(this.type){
		case NULL:
		case SELF:
			break;
		case DOUBLE:
			if(d == null)
				throw new IllegalArgumentException(error+" expects 1 argument of type double");
			break;
		case BOOLEAN:
			if(b == null)
				throw new IllegalArgumentException(error+" expects 1 argument of type boolean");
			break;
		case GETBOOLEAN:
		case GETDOUBLE:
		case GETENTITY:
			if(variable == null)
				throw new IllegalArgumentException(error+" expects a variable name of type String");
			break;
		case NOT:
			if(e1.getType().cls() != Boolean.class)
				throw new IllegalArgumentException(error+" expects 1 expression with return type Boolean");
			break;
		case SQRT:
		case SIN:
		case COS:
		case SEARCH:
			if(e1.getType().cls() != Double.class)
				throw new IllegalArgumentException(error+" expects 1 expression with return type Double");
			break;
		case GETX:
		case GETY:
		case GETRADIUS:
		case GETDIR:
		case GETAP:
		case GETMAXAP:
		case GETHP:
		case GETMAXHP:
		case SAMETEAM:
		case ISFOOD:
		case ISWORM:
			if(e1.getType().cls() != Entity.class)
				throw new IllegalArgumentException(error+" expects 1 expression with return type Entity");
			break;
		case AND:
		case OR:
			if(e1.getType().cls() != Boolean.class || e2.getType().cls() != Boolean.class)
				throw new IllegalArgumentException(error+" expects 2 expressions with return type Boolean");
			break;
		case LESS:
		case LESSOREQUAL:
		case GREATER:
		case GREATEROREQUAL:
		case ADD:
		case SUBTRACT:
		case MULTIPLY:
		case DIVISION:
			if(e1.getType().cls() != Double.class || e2.getType().cls() != Double.class)
				throw new IllegalArgumentException(error+" expects 2 expression with return type Double");
			break;
		case EQUAL:
			if(e1 == null || e2 == null)
				throw new IllegalArgumentException(error+" expects 2 non null expressions");
			break;
		}
	}
	
	/**
	 * @return the type
	 */
	public ExpressionType getType() {
		return type;
	}
	
	/**
	 * We suppress all warnings, because the compiler complains that we do unchecked type casting. This checking
	 * happens on construction with the create... methods.
	 * @param program
	 * @return
	 * @throws IllegalStateException
	 */
	@SuppressWarnings("unchecked")
	public T execute(Program program) throws IllegalStateException{
		if(type.cls() == Boolean.class){
			Boolean result;
			switch(type){
			case BOOLEAN:
				result =  b;
				break;
			case AND:
				result = (Boolean) e1.execute(program).get() && (Boolean) e2.execute(program).get();
				break;
			case OR:
				result = (Boolean) e1.execute(program).get() || (Boolean) e2.execute(program).get();
				break;
			case NOT:
				result = !(Boolean) e1.execute(program).get();
				break;
			case ISWORM:
				result = e1.execute(program).get() instanceof Worm;
				break;
			case ISFOOD:
				result = e1.execute(program).get() instanceof Food;
				break;
			case LESS:
				result = (Double) e1.execute(program).get() < (Double) e2.execute(program).get();
				break;
			case LESSOREQUAL:
				result = (Double) e1.execute(program).get() <= (Double) e2.execute(program).get();
				break;
			case GREATER:
				result = (Double)  e1.execute(program).get() > (Double) e2.execute(program).get();
				break;
			case GREATEROREQUAL:
				result = (Double) e1.execute(program).get() >= (Double) e2.execute(program).get();
				break;
			case EQUAL:
				result = e1.execute(program).get() == e2.execute(program).get();
				break;
			case SAMETEAM:
				result = sameTeam(program,(Expression<BNFEntity>) e1);
				break;
			case GETBOOLEAN:
				result = ((BNFBoolean) program.getGlobals().get(variable)).get();
				break;
			default:
				throw new IllegalStateException("ExpressionType "+type+" with return type "+type.cls().getSimpleName() +" was not found");
			}
			
			return (T) new BNFBoolean(result);
			
		}else if(type.cls() == Double.class){
			Double result;
			switch(type){
			case DOUBLE:
				result = d;
				break;
			case GETX:
				result = ((Entity) e1.execute(program).get()).getPosition().getX();
				break;
			case GETY:
				result = ((Entity) e1.execute(program).get()).getPosition().getY();
				break;
			case GETRADIUS:
				result = ((Entity) e1.execute(program).get()).getRadius();
				break;
			case GETDIR:
				result = ((Worm) e1.execute(program).get()).getDirection();
				break;
			case GETAP:
				result = (double) ((Worm) e1.execute(program).get()).getActionPointsCurrent();
				break;
			case GETMAXAP:
				result = (double) ((Worm) e1.execute(program).get()).getActionPointsMax();
				break;
			case GETHP:
				result = (double) ((Worm) e1.execute(program).get()).getHitPointsCurrent();
				break;
			case GETMAXHP:
				result = (double) ((Worm) e1.execute(program).get()).getHitPointsMax();
				break;
			case ADD:
				result = (Double)  e1.execute(program).get() + (Double) e2.execute(program).get();
				break;
			case SUBTRACT:
				result = (Double)  e1.execute(program).get() - (Double) e2.execute(program).get();
				break;
			case MULTIPLY:
				result = (Double)  e1.execute(program).get() * (Double) e2.execute(program).get();
				break;
			case DIVISION:
				result = (Double)  e1.execute(program).get() / (Double) e2.execute(program).get();
				break;
			case SQRT:
				result = Math.sqrt((double) e1.execute(program).get());
				break;
			case SIN:
				result = Math.sin((double) e1.execute(program).get());
				break;
			case COS:
				result = Math.cos((double) e1.execute(program).get());
				break;
			case GETDOUBLE:
				result = ((BNFDouble) program.getGlobals().get(variable)).get();
				break;
			default:
				throw new IllegalStateException("ExpressionType "+type+" with return type "+type.cls().getSimpleName() +" was not found");
			}
			
			return (T) new BNFDouble(result);
			
		}else if(type.cls() == Entity.class){
			Entity result;
			switch(type){
			case SELF:
				result = program.getWorm();
				break;
			case NULL:
				result = null;
				break;
			case GETENTITY:
				result = ((BNFEntity) program.getGlobals().get(variable)).get();
				break;
			case SEARCH:
				result = program.getWorm().getWorld().searchObject(program.getWorm(), (Double) e1.execute(program).get());
				break;
			default:
				throw new IllegalStateException("ExpressionType "+type+" with return type "+type.cls().getSimpleName() +" was not found");
			}
			
			return (T) new BNFEntity(result);
		}
		throw new IllegalStateException("ExpressionType "+type+" with return type "+type.cls().getSimpleName() +" was not found");
	}
	
	private boolean sameTeam(Program program, Expression<BNFEntity> e){
		Worm w = program.getWorm(), w2 = (Worm) e.execute(program).get();
		if(w2 == null)
			return false;
		else{
			Team t1 = w.getTeam(), t2 = w2.getTeam();
			if(t1 == null || t2 == null)
				return false;
			return t1 == t2;
			
		}
	}
}
