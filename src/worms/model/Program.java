package worms.model;

import java.util.Map;
import java.util.Stack;

import worms.gui.game.IActionHandler;
import worms.model.programs.*;

/**
 * Represents a program that a worm can execute.
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class Program {

	private final Map<String,BNFObject> globals;
	private final IActionHandler handler;
	private Statement body;
	private Worm worm;

	private int statementCounter = 0;
	private Stack<Statement> stack;
	
	protected Program(Map<String,BNFObject> globals, Statement body, IActionHandler handler){
		this.globals = globals;
		this.handler = handler;
		this.body = body;
		this.stack = new Stack<Statement>();
	}

	protected Program(Map<String,BNFObject> globals, Statement body, Worm worm, IActionHandler handler) {
		this(globals,body,handler);
		this.worm = worm;
	}

	/**
	 * @return the globals
	 */
	public Map<String, BNFObject> getGlobals() {
		return globals;
	}

	/**
	 * @return the worm
	 */
	public Worm getWorm() {
		return worm;
	}

	/**
	 * @return the handler
	 */
	public IActionHandler getHandler() {
		return handler;
	}

	public boolean isWellFormed() {
		return body.isWellFormed(false);
	}

	/**
	 * @param worm the worm to set
	 */
	protected void setWorm(Worm worm) {
		this.worm = worm;
	}

	
	protected void execute(){
		if(!this.isWellFormed())
			return;
		this.statementCounter = 0;
		try{
			if(stack.isEmpty())
				body.execute(this);
			while(!stack.isEmpty()){
				stack.pop().execute(this);
			}
		}catch (StatementInterruptException ex){
			System.out.println("StatementInterruptException: " + this.getWorm()+" : "+ex.getMessage());
		}catch (RuntimeException ex){
			System.out.println("RuntimeException: " + this.getWorm().toString()+" : "+ex);
		}	
	}

	
	public void push(Statement s) throws StatementInterruptException{
		stack.push(s);
		statementCounter++;
		if(statementCounter++ >= 1000){
			statementCounter = 0;
			throw new StatementInterruptException("Execution overflow");
		}
	}

	
	public void pop(){
		stack.pop();
	}


}
