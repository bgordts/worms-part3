package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Basic game object that can jump.
 * 
 * @invar 	The direction of this Entity must be valid if the Entity is active
 *		  | if(entity.isActive())
 * 		  |		isValidDirection(getDirection()) == true
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public abstract class JumpableEntity extends Entity{

	public static final double g = 9.80665;

	private double direction;

	/**
	 * Constructor for JumpableEntity.
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	direction
	 * 			the direction to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	world
	 * 			the world where this entity lives in
	 * 
	 * @effect	this JumpableEntity is initialized as an Entity with an x, y position, a radius and a World.
	 * 		  | super(x,y,radius, world)
	 * @effect	the direction will be set to direction if it's valid
	 * 		  | this.setDirection(direction)
	 */
	@Raw
	protected JumpableEntity(double x, double y, double direction, double radius, World world) {
		super(x,y,radius, world);

		direction = direction % (2*Math.PI);
		direction = (direction < 0)?direction+2*Math.PI:direction;
		this.setDirection(direction);
	}

	/**
	 * Get the direction of the entity.
	 * 
	 * @return	the direction of the entity
	 * 		  |	result == this.direction
	 */
	@Basic
	public double getDirection() {
		return this.direction;
	}

	/**
	 * Get the mass of this entity.
	 * 
	 * @return 	the mass of this entity
	 */
	public abstract double getMass();

	/**
	 * Get the force that is exerted on this entity to let it jump.
	 * 
	 * @return	the force of the jump
	 */
	public abstract double getJumpForce();

	/**
	 * Calculate the initial speed of this entity were it to jump
	 * 
	 * @return 	the initial speed of the jump
	 * 		 | 	result == (this.getJumpForce()/this.getMass())*0.5
	 */
	public double getInitialJumpVelocity(){
		return (this.getJumpForce()/this.getMass())*0.5;
	}	

	/**
	 * Check if the given direction is valid.
	 * 
	 * @param 	direction
	 * 			direction to check
	 * 
	 * @return	returns true if all the conditions for a valid direction are met: it must be a valid number, it 
	 * 			must lie between 0 and 2 times Pi
	 * 		  | result == (!Double.isNaN(direction)) && (0 <= direction) && (direction <= 2*Math.PI)
	 */
	public static boolean isValidDirection(double direction){
		return !Double.isNaN(direction) && 0 <= direction && direction <= 2*Math.PI;
	}

	/**
	 * Check if the given jump force is valid.
	 * 
	 * @param 	force
	 * 			jump force to check
	 * 
	 * @return	returns true if all the conditions for a valid jump force are met: it must be a valid number 
	 * 			and it must be greater than zero
	 * 		  | result == (!Double.isNaN(force)) && (force >= 0)
	 */
	public static boolean isValidJumpForce(double force){
		return !Double.isNaN(force) && force >= 0;
	}

	/**
	 * Check if the given time step is valid.
	 * 
	 * @param 	timeStep
	 * 			time step to check
	 * 
	 * @return	returns true if all the conditions for a valid time step are met: it must be a valid number 
	 * 			and it must be greater than zero
	 * 		  | result == (!Double.isNaN(timeStep)) && (timeStep >= 0)
	 */
	public static boolean isValidTimeStep(double timeStep){
		return !Double.isNaN(timeStep) && timeStep >= 0;
	}

	/**
	 * Is this JumpableEntity able to jump?
	 * 
	 * @return 	true if this JumpableEntity can jump.
	 */
	public abstract boolean canJump(double timeStepInterval);

	/**
	 * Can this entity jump through this position?
	 * 
	 * @return	true if this Entity can jump through the given position
	 * 		  |	result == this.getWorld().isPassable(position,this.getRadius())
	 */
	public boolean isJumpablePosition(Position position){
		return this.getWorld().isPassable(position,this.getRadius());
	}

	/**
	 * Let this entity jump. It will position this entity at the calculated after-jump location.
	 * 
	 * First the total jump time is calculated with the jumpTime method. Then the final position is
	 * calculated with jumpStep, using the total time needed to complete the jump. This final position is
	 * than used to set this Entity's position.
	 * 
	 * @param	timeStepInterval
	 * 			The precision the calculations are done with. 
	 * 
	 * @effect	the position of this entity will change to the calculated position just before the entity hitted something, 
	 * 			or to the position where it hits something for the first time in the slope.
	 *    	  | this.setPosition(jumpStep(jumpTime(timeStepInterval))) || this.setPosition(jumpStep(jumpTime(timeStepInterval) + timeStepInterval))
	 * @effect	destroy this entity if it located at an illegal position after the jump
	 * 		  | catch(IllegalArgumentException)
	 *    	  | 	this.destroy()
	 *    
	 * @throws	IllegalMovementException
	 * 			This entity cannot jump
	 * 		  | !this.canJump(timeStepInterval)
	 */
	protected void jump(double timeStepInterval) throws IllegalMovementException {
		if(!this.canJump(timeStepInterval))
			throw new IllegalMovementException("The entity is not able to jump.");

		this.setPosition(jumpStep(jumpTime(timeStepInterval)));
	}

	/**
	 * Time needed to complete a jump started from the current position till we hit something.
	 * 
	 * Sample the curve that describes the jump with the given timeStepInterval precision. Returns
	 * the time that the jump lasts till the entity hits something. The entity hit something if
	 * this.isJumpablePosition(Position hitposition) == false.
	 * 
	 * @param	timeStepInterval
	 * 			The precision the calculations are done with. 
	 * 
	 * @return	time minus the timeStepInterval since the start of the jump when the first position is hit where we can't jump through
	 * 		 |	for(i = 0; i < result; i++)
	 * 		 |		this.isJumpablePosition(i) == true
	 * 		 |	&&
	 * 		 |	this.isJumpablePosition(result + timeStepInterval) == false
	 * 
	 * @throws	IllegalArgumentException
	 * 			The given timeStepInterval is not valid
	 * 		  | !Jumpable.isValidTimeStep(timeStepInterval)
	 */
	protected double jumpTime(double timeStepInterval) throws IllegalArgumentException{
		if(!JumpableEntity.isValidTimeStep(timeStepInterval))
			throw new IllegalArgumentException("Timestep must be a valid Double, may not be negative or may not be infinity");

		double timeStep = 0;
		Position nextStep;

		//Just calculate new positions till we hit impassable terrain
		do{			
			timeStep += timeStepInterval;
			nextStep = jumpStep(timeStep);	

			//This handles falling out of the map correctly. The jumpable entity will now leave the map, and than be destroyed. Otherwise,
			//this entity would stop just before leaving the map and therefore be positioned on a not allowed position.
			if(!this.getWorld().isInWorld(nextStep,this.getRadius())){
				return timeStep;
			}
		}while(this.isJumpablePosition(nextStep));

		return timeStep - timeStepInterval;
	}

	/**
	 * Returns the in-jump position at the given timeStep from a jump launched at the current position.
	 * 
	 * @param	timeStep
	 * 			the time at which to calculate the position
	 * 
	 * @return	The in-jump position after the given time.
	 * 		  |	v = getInitialJumpVelocity()
	 *	 	  |	x = this.getPosition().getX()
	 *		  |	y = this.getPosition().getY()
	 *		  |	vX = v*Math.cos(this.getDirection())
	 *		  |	vY = v*Math.sin(this.getDirection())
	 * 		  | result == new Position(x + (vX*timeStep),  y + (vY*timeStep -0.5*g*timeStep*timeStep))
	 * 
	 * @throws	IllegalArgumentException
	 * 			The given timeStepInterval is not valid
	 * 		  | !Jumpable.isValidTimeStep(timeStepInterval)
	 */
	protected Position jumpStep(double timeStep) throws IllegalArgumentException{		
		if(!JumpableEntity.isValidTimeStep(timeStep))
			throw new IllegalArgumentException("Timestep must be a valid Double, may not be negative or may not be negative infinity");

		double v = getInitialJumpVelocity();

		double x = this.getPosition().getX();
		double y = this.getPosition().getY();
		double vX = v*Math.cos(this.getDirection());
		double vY = v*Math.sin(this.getDirection());

		x = x + (vX*timeStep);
		y = y + (vY*timeStep -0.5*g*timeStep*timeStep);

		Position newPosition = new Position(x,  y);

		return newPosition;
	}

	/**
	 * Set the direction the Entity faces
	 * 
	 * @param 	direction
	 * 			the direction to set to
	 * 
	 * @pre		the direction must be valid
	 * 		  | isValidDirection(direction) == true
	 * 
	 * @post	If the direction is Valid, the direction of this entity will be set to the given direction.
	 * 		  | new.direction == direction
	 */
	@Raw
	protected void setDirection(double direction) {
		assert(isValidDirection(direction));
		this.direction = direction;
	}
}
