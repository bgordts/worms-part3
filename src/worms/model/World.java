package worms.model;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import be.kuleuven.cs.som.annotate.*;

/**
 * Represents the game's world.
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class World {

	//Dimensions of the world
	@Model
	final private double width;
	@Model
	final private double height;
	//Conversion factor from meters to pixels
	@Model
	final private double xscale;
	@Model
	final private double yscale;

	//Indicates whether a pixel at position x,y is passable or not
	@Model
	final private boolean[][] passableMap;

	//Random number generator that was given
	final private Random random;

	//A LinkedHashSet for each set of classes we need to track. The order is important for taking turns
	private WormCycle worms;
	private HashSet<Food> foods;
	@Model
	private HashSet<Team> teams;
	private Projectile activeProjectile;

	//Is the game in started
	private boolean started = false;
	//Name of winner if there is one
	private String winner = null;
	//Set of winners
	private HashSet<Worm> winners = null;

	
	private class WormCycle extends LinkedList<Worm>{
		
		private static final long serialVersionUID = 8020969271749893549L;
		
		@Override
		public Worm poll(){
			Worm w = super.poll();
			super.add(w);
			return w;
		}
		
		@Override
		public boolean add(Worm e){
			if(this.contains(e))
				return false;
			return super.add(e);
		}
	}
	/**
	 * Construct a new world
	 * 
	 * @param width	width of the new world
	 * @param height height of the new world
	 * @param passableMap map to know which positions are passable
	 * @param random random number generator so worlds are replicable
	 * 
	 * @post the width of the world is set to width
	 * 	   | this.width == width
	 * @post the height of the world is set to height
	 * 	   | this.height == height
	 * 
	 * @post if passableMap is null, the whole world is impassable
	 * 	   | if(passableMap == null)
	 * 	   | 	this.passableMap == new boolean[][]{{false}}
	 * @post the passableMap is set to passableMap, but flipped upside down for convenience later
	 * 	   | for i from 0 to passableMap.length
	 *	   | 	for j from 0 to passableMap[0].length
	 *	   |		this.passableMap[i][j] == passableMap[passableMap.length-i-1][j];
	 *
	 * @post the conversion factor from meters to pixels in both x and y is set
	 * 	   | this.xscale == passableMap[0].length/this.width
	 * 	   | this.yscale == passableMap.length/this.height
	 * 
	 * @post the sets of entities are initialized
	 * 	   | new.getFoods() == new HashSet&lt;Food>()
	 * 	   | new.getWorms() == new LinkedHashSet&lt;Worm>()
	 * @post the set of teams is initialized
	 * 	   | this.teams == new HashSet&lt;Team>()
	 * 
	 * 
	 * @throws IllegalArgumentException the dimensions of the map must be valid
	 * 		 | isValidDimensions(width,height)
	 * 
	 * 
	 * -----------------------------------------------
	 * @post the random number generator is set
	 * 	   | this.random == random
	 * 
	 * 
	 */
	protected World(double width, double height, boolean[][] passableMap, Random random) throws IllegalArgumentException{
		if(!isValidDimensions(width,height))
			throw new IllegalArgumentException("Dimensions given are not valid");
		this.width = width;
		this.height = height;

		//Story of how the map got flipped turned upside down
		if(passableMap == null)
			this.passableMap = new boolean[][]{{false}};
		else{
			this.passableMap = new boolean[passableMap.length][passableMap[0].length];
			for(int i = 0; i < passableMap.length; i++){
				for(int j = 0; j < passableMap[0].length;j++){
					this.passableMap[i][j] = passableMap[passableMap.length-i-1][j];
				}
			}
		}
		if(this.width == 0)
			this.xscale = 1;
		else
			this.xscale = this.passableMap[0].length/this.width;

		if(this.width == 0)
			this.yscale = 1;
		else
			this.yscale = this.passableMap.length/this.height;

		if(random == null)
			random = new Random();
		this.random = random;

		this.foods = new HashSet<Food>();
		this.worms = new WormCycle();
		this.teams = new HashSet<Team>();

	}

	/**
	 * Start the game
	 * @return whether it was successful
	 */
	protected boolean startGame() {
		if(worms.size() == 0)
			return false;
		Worm last = worms.pollLast();
		worms.addFirst(last);
		this.started = true;
		this.winner = null;
		this.winners = null;
		return true;
	}

	/**
	 * Next turn
	 */
	protected void nextTurn() {
		
		if(this.isGameFinished())
			return;
		worms.poll();
		this.setActiveProjectile(null);
		this.getActiveWorm().playTurn();
	}

	/**
	 * Check if the game is finished
	 * 
	 * @post if the game is finished, the name of the winner or winning team will be stored if there is one
	 * 
	 * @post if there is no winner (because everybody is dead) the name of the winner is null
	 * 
	 * @return false if there is more than 1 team and/or solo worm surviving, true otherwise
	 *  		
	 */
	public boolean isGameFinished(){
		if(!this.started){
			return true;
		}

		Team team = null,wormTeam;
		Worm survivor = null;
		int counter = 0;
		for(Worm worm :this.getWorms()){
			if(worm.getHitPointsCurrent() != 0 && worm.isActive()){
				wormTeam = worm.getTeam();
				if(wormTeam == null){
					survivor = worm;
					counter++;
				} else {
					if(team == null){
						team = wormTeam;
						counter++;
					} else if(team == wormTeam)
						;
					else
						counter++;
				}
			}
			if(counter>1){
				return false;
			}
		}

		if(counter == 1){
			if(team != null){
				this.winner = team.getName();
				this.winners = team.getLive();
			}
			else
			{
				this.winner = survivor.getName();
				this.winners = new HashSet<Worm>();
				this.winners.add(survivor);
			}
		}

		this.started = false;
		return true;
	}

	/**
	 * Get the name of winner or winning team, null if there is none
	 * 
	 * @return name of winner or winning team
	 */
	@Basic
	public String getWinner(){
		return this.winner;
	}

	/**
	 * Get the winning worms
	 * 
	 * @return the worms that have won
	 */
	@Basic
	public HashSet<Worm> getWinners(){
		return (HashSet<Worm>) this.winners.clone();
	}


	/**
	 * Get all the foods in this world
	 * 
	 * @return the foods
	 */
	@Basic
	public HashSet<Food> getFoods() {
		return (HashSet<Food>) foods.clone();
	}

	/**
	 * Get all the worms in this world
	 * 
	 * @return the worms
	 */
	@Basic
	public LinkedList<Worm> getWorms() {
		return (LinkedList<Worm>) worms.clone();
	}

	/**
	 * Returns the active worm if there is one, otherwise null
	 * 
	 * @return the active worm
	 */
	@Basic
	public Worm getActiveWorm() {
		return worms.peek();
	}

	/**
	 * Returns the active projectile if there is one, otherwise null
	 * 
	 * @return the active projectile
	 */
	@Basic
	public Projectile getActiveProjectile() {
		return activeProjectile;
	}

	/**
	 * Set the active projectile
	 * 
	 * @param projectile the projectile to set
	 * 
	 * @post the active projectile is set to projectile
	 * 		|new.getActiveProjectile() == projectile;
	 */
	private void setActiveProjectile(Projectile projectile) {
		this.activeProjectile = projectile;
	}

	/**
	 * Check if the dimensions are valid
	 * 
	 * @param width	width of the world
	 * @param height height of the world
	 * 
	 * @return true if the dimensions are valid
	 * 		 | result == (width > 0 && width != Double.NaN && height > 0 && height != Double.NaN)
	 */
	@Model
	private boolean isValidDimensions(double width, double height){
		return (0 <= width && width != Double.NaN && 0 <= height && height != Double.NaN);
	}

	/**
	 * Check if a given position is inside the world
	 * 
	 * @param pos the position to check
	 * 
	 * @return whether x and y are valid positions, and if they are within width and height
	 * 		 | result == (ModelUtil.isValidNumber(x) && ModelUtil.isValidNumber(y) &&   0 <= x && x <= width && 0 <= y && y <= height)
	 */
	public boolean isInWorld(Position pos, double radius){
		double x = pos.getX(), y = pos.getY();
		return (ModelUtil.isValidNumber(x) && ModelUtil.isValidNumber(y) &&   0 <= x - radius && x + radius <= width && 0 <= y - radius && y + radius <= height);
	}


	/**
	 * Add a team to the world
	 * 
	 * @param name the name of the team to add
	 * 
	 * @post add a new team to this.teams
	 * 
	 * @throws IllegalArgumentException if the name of the team is not valid
	 * 		 | Team.isValidName(name)
	 * @throws IllegalArgumentException if there are already 10 teams
	 * 		 | this.teams.size() <= 10
	 */
	protected void addTeam(String name) throws IllegalArgumentException{
		if(this.teams.size() > 10)
			throw new IllegalArgumentException("Too many teams");
		Team team = new Team(name);
		teams.add(team);


	}

	/**
	 * Spawn a new entity in this world, of class Food or of class Worm
	 * 
	 * @param c the class to spawn: 'f' spawns Food, 'w' spawns Worm
	 * 
	 * @post if c was 'f' then a new food must have been spawned
	 * 		| if(c == 'f')
	 * 		| 	then new.getFoods().size() = new.getFoods().size()+1
	 * @post if c was 'w' then a new worm must have been spawned
	 * 		| if(c == 'w')
	 * 		|	then new.getWorms().size() == this.getWorms().size()+1
	 * @post if c was 'w' and there are teams in the world, then the spawned worm will be added to a team
	 * 		| if(c == 'w' && !teams.isEmpty())
	 * 		| 	then ent.getTeam() != null
	 * 
	 * @throws IllegalArgumentException c must be either 'f' or 'w'
	 * 		 | !(c == 'f' || c == 'w')
	 * @throws IllegalStateException if the game is underway
	 * 		 | !this.isGameFinished()
	 * @throws IllegalStateException if we can't fit the entity in the world
	 * 		 | tries > 2^20
	 * 
	 */
	protected void spawnEntity(char c, Program program) throws IllegalArgumentException, IllegalStateException{
		if(!this.isGameFinished())
			throw new IllegalStateException("The game is underway!");

		double radius;
		Entity ent;

		if(c == 'f'){
			radius = Food.getFoodRadius();
		} else if (c == 'w'){
			radius = random.nextDouble()/20 + .25;
		} else throw new IllegalArgumentException("f for food or w for worm, nothing else");

		double rx,ry;
		Position p;
		int tries = 0,maxtries = 1<<20;
		do{
			rx = random.nextDouble()*this.width;
			ry = random.nextDouble()*this.height;
			p = new Position(rx,ry);
			tries++;
			if(tries++ > maxtries)
				throw new IllegalStateException("Couldn't fit entity into world");
				


		}while(!(isAdjacent(p,radius) && this.isInWorld(p, radius)));

		if(c == 'f')
			ent = new Food(p.getX(),p.getY(),this);
		else{

			double dir = random.nextDouble()*2*Math.PI;
			String name = "Worm "+Integer.toString(worms.size()+1);
			ent = new Worm(p.getX(),p.getY(),dir,radius,name,this,program);

			//If there are teams, add the worm to the smallest team
			if(!teams.isEmpty()){
				Iterator<Team> teamer = teams.iterator();
				Team min = teamer.next(); int m = min.getTeam().size();
				for(Team team : teams){
					if(team.getTeam().size() < m){
						min = team;
						m = min.getTeam().size();
					}
				}
				min.add((Worm) ent);
			}
		}

	}

	/**
	 * Add a given entity to this world
	 * 
	 * @param newEntity the entity to add
	 * 
	 * @post according to its class the entity is assigned to the world
	 * 		|if(newEntity instanceof Worm)
	 * 		|	then new.getWorms().contains(newEntity)
	 * 		|else if(newEntity instanceof Food)
	 * 		|	then new.getFoods().contains(newEntity)
	 * 		|else if(newEntity instanceof Projectile
	 * 		|	then new.getActiveProjectile() == newEntity
	 * 
	 * @throws IllegalArgumentException the entity must be of class Food, Worm or Projectile
	 * 		 | !(newEntity instanceof Worm || newEntity instanceof Food || newEntity instanceof Projectile)
	 * 
	 * @throws IllegalArgumentException the entity may not already exist in this world, except if it is a Projectile, then
	 * 		   the active projectile will be replaced
	 * 		 | this.worms.contains(newEntity) || this.foods.contains(newEntity)
	 */
	protected void addEntity(Entity newEntity) throws IllegalArgumentException{
		if(!(newEntity instanceof Worm || newEntity instanceof Food || newEntity instanceof Projectile))
			throw new IllegalArgumentException("Entity must be of type food or worm.");

		boolean success = false;
		if(this.isGameFinished()){
			if(newEntity instanceof Worm)
				success = worms.add((Worm) newEntity);
			else if(newEntity instanceof Food)
				success = foods.add((Food) newEntity);
		} else if(newEntity instanceof Projectile){
			if(this.getActiveProjectile() != null)
				this.removeEntity(this.getActiveProjectile());
			this.setActiveProjectile((Projectile) newEntity);
			success = true;
		} else throw new IllegalArgumentException("Error adding entity to world: The game may not be underway if adding food or worms, and it must be underway if adding projectiles");
		if(!success)
			throw new IllegalArgumentException("Error adding entity to world");
	}

	/**
	 * Checks whether this world contains the given entity
	 * 
	 * @param entity
	 * 
	 * @return true if the entity is in this world
	 * 		 | result == (foods.contains(entity)||worms.contains(entity)||this.getActiveProjectile() == entity)
	 */
	public boolean contains(Entity entity){
		return (foods.contains(entity)||worms.contains(entity)||this.getActiveProjectile() == entity);
	}

	/**
	 * Remove an entity from this world
	 * 
	 * @param entity the entity to delete
	 * 
	 * @post the entity is deleted
	 * 		|if(entity instanceof Worm)
	 * 		| 	then !new.getWorms().contains(entity)
	 * 		|else if(entity instanceof Food)
	 * 		|	then !new.getFoods().contains(entity)
	 * 		|else if(entity == this.getActiveProjectile())
	 * 		|	then new.getActiveProjectile() == null
	 * 
	 * @throws IllegalArgumentException The entity must exist in this world
	 * 		 | !(this.getFoods().contains(entity) || this.getWorms().contains(entity) || entity == this.getActiveProjectile)
	 * 		
	 */
	protected void removeEntity(Entity entity) throws IllegalArgumentException{
		boolean success = false;
		if(entity instanceof Worm){
			boolean isActiveWorm = entity == this.getActiveWorm();
			success = worms.remove(entity);		
			if(isActiveWorm){
				if(((Worm) entity).getProgram() == null)
					this.nextTurn();
				else
					throw new IllegalStateException("The computer killed itself, go to next turn");
			}
		} else if(entity instanceof Food){
			success = foods.remove((Food) entity);
		}else if(entity instanceof Projectile && entity == this.getActiveProjectile()){
			this.setActiveProjectile(null);
			success = true;
		}
		if(!success)	
			throw new IllegalArgumentException("This entity does not exist in this world.");
	}

	/**
	 * Checks whether in a radius radius around position p there are inpassable pixels
	 * @param p	center of the circle to check
	 * @param radius the radius of the circle to check
	 * 
	 * @post if p isn't a valid position, set p to -1,-1
	 * 		| if !(ModelUtil.isValidNumber(p.getX()) || ModelUtil.isValidNumber(p.getY()))
	 * 		| 	then p == new Position(-1,-1)
	 * @post if radius isn't a valid number, set radius to 0
	 * 		| if !ModelUtil.isValidNumber(radius)
	 * 		| 	then radius == 0
	 * @post if radius is negative, make it positive
	 * 		| if radius < 0
	 * 		| 	then radius == -radius
	 * 
	 * @return whether the area is passable or not
	 * 		| if and only if result == true
	 * 		| 	ArrayList[] circle = World.drawEllipse(p, radius, this.xscale, this.yscale);
	 *		| 	for each y in circle[0]
	 *		|	  	if(0 <= y && y < this.passableMap.length)
	 *		| 		  	for each x from circle[1][0] to circle[1][0]
	 *		|			  	if(0 <= x && x < this.passableMap[0].length)
	 *		|				  	this.passableMap[y][x] == true
	 */
	public boolean isPassable(Position p, double radius){
		if(!(ModelUtil.isValidNumber(p.getX()) || ModelUtil.isValidNumber(p.getY())))
			p = new Position(-1,-1);
		if(!ModelUtil.isValidNumber(radius))
			radius = 0;
		if(radius < 0)
			radius = -radius;

		ArrayList[] circle = World.drawEllipse(p, radius, this.xscale, this.yscale);
		ArrayList<Integer> cy = circle[0];
		ArrayList<Integer[]> cx = circle[1];

		boolean passable = true;
		for(int i = 0; i < cy.size(); i++){
			int y = cy.get(i);
			Integer[] x = cx.get(i);
			if(0 <= y && y < this.passableMap.length)
				for(int j = x[0]; j <= x[1]; j++)
					if(0 <= j && j < this.passableMap[0].length)
						passable &= this.passableMap[y][j];
			if(!passable)
				return false;
		}

		return passable;
	}

	/**
	 * Check if the area around position p is adjacent to impassable terrain
	 * @param p	the position to check
	 * @param radius the radius to check
	 * @return true if the area around p is passible in radius, but not in 1.1*radius
	 * 		 | result == (this.isPassable(p,radius) && !this.isPassable(p,radius*1.1))
	 */
	public boolean isAdjacent(Position p, double radius){
		return(this.isPassable(p,radius) && !this.isPassable(p,radius*1.1));
	}

	/**
	 * Get the maximal possible distance the entity can travel along the given line
	 * 
	 * @param entity the entity to move
	 * @param b the position to move to
	 * 
	 * @return the farthest position along the line given, finding the furthest passable and
	 * 		   the furthest adjacent positions. Returns the former only if it is twice as far as the latter. If the maximal
	 * 		   distance is 0, return null.
	 * 		|	for(int i = 0; i <= numSteps; i++){
	 *		|		p = new Position(x0+xstep*i,y0+ystep*i);
	 *		|		if(this.isAdjacent(p, radius)){
	 *		|			maxp1 = p;
	 *		|			m1 = i;
	 *		|		}
	 *		|	}
	 *		|
	 *		|	for(int j = 0; j <= numSteps; j++){
	 *		|		p = new Position(x0+xstep*j,y0+ystep*j);
	 *		|		if(this.isPassable(p, radius)){
	 *		|			maxp2 = p;
	 *		|			m2 = j;
	 *		|		}
	 *		|	}
	 *		|
	 *		|	if(m1 == 0 && m2 == 0){
	 *		|		result == null;
	 *		|	} else if(m2 > 2*m1){
	 *		|		result == maxp2;
	 *		|	} else {
	 *		|		result ==  maxp1;
	 *		|	}
	 * 		 
	 * 			
	 */
	protected Position getMaxMoveDistance(Entity entity, Position b){
		/*
		 * We check the every position along the given line to find the furthest passable and furthest adjacent position
		 * if there is none, return null
		 * otherwise return the
		 */
		double radius = entity.getRadius();
		double x0 = entity.getPosition().getX();
		double y0 = entity.getPosition().getY();
		double x1 = b.getX();
		double y1 = b.getY();

		double deltax = Math.abs(x1-x0);
		double deltay = Math.abs(y1-y0);
		double dsum = deltax+deltay;
		double step = (deltax/this.xscale+deltay/this.yscale)/dsum;
		double distance = Math.sqrt(deltax*deltax+deltay*deltay);
		int numSteps = Math.max((int) (4*distance/step),1<<8);

		double xstep = (x1-x0)/numSteps;
		double ystep = (y1-y0)/numSteps;

		Position maxp1 = null,maxp2 = null, p = new Position(x0,y0);
		int m1 = 0, m2 = 0;


		for(int i = 0; i <= numSteps; i++){
			p = new Position(x0+xstep*i,y0+ystep*i);
			if(this.isAdjacent(p, radius)){
				maxp1 = p;
				m1 = i;
			}
		}

		for(int j = 0; j <= numSteps; j++){
			p = new Position(x0+xstep*j,y0+ystep*j);
			if(this.isPassable(p, radius)){
				maxp2 = p;
				m2 = j;
			}
		}

		if(m1 == 0 && m2 == 0){
			return null;
		} else if(m2 > 2*m1){
			return maxp2;
		} else {
			return maxp1;
		}
	}

	/**
	 * Get the maximum distance an entity can fall
	 * 
	 * @param entity the entity to check
	 * @return the furthest position the entity can fall
	 * 		| 	double step = (1-1e-6)/(4*this.yscale);
	 * 		|	for each Position p from entity.getPosition to result
	 * 		|		p.isPassable
	 * 		|	Position p = new Position(result.getX(), result.getY() - step)
	 * 		| 	!p.isPassable
	 */
	protected Position getMaxFallDistance(Entity entity){

		double step = Math.min((1-1e-6)/(4*this.yscale),this.height/(1<<8));
		double radius = entity.getRadius();

		Position t = entity.getPosition();
		Position p = new Position(t.getX(),t.getY());
		
		while(this.isPassable(p, radius)){
			if(p.getY()< 0)
				return p;
			p.setY(p.getY()-step);
		}
		double y = p.getY()+step;
		p.setY(Math.round(2*p.getY()*this.yscale)/this.yscale/2);
		if(this.isPassable(p, radius))
			return p;

		p.setY(y);

		if(p.getY() == t.getY())
			return null;
		return p;

	}

	/**
	 * Function that calculates the pixels along the ellipse defined by the given position and radius in the game world
	 * with xscale and yscale the scaling factor to pixels along the x-axis and y-axis respectively
	 * 
	 * @param position the midpoint of the cirle
	 * @param radius the radius of the circle before stretching
	 * @param xscale the stretching factor along the x-axis
	 * @param yscale the stretching factor along the y-axis
	 * 
	 * @return two arrays: one with the y coordinates, and one with the minimum and maximum x coordinates
	 * 		   for that y coordinate, in pixels
	 * 		 | for each y in result[0]
	 * 		 | 		for each {x0,x1} in result[1]
	 * 		 | 			(x0/xscale)^2 + (y/yscale)^2 == radius^2 && (x1/xscale)^2 + (y/yscale)^2 == radius^2 && x0 <= x1
	 */
	@Model
	private static ArrayList[] drawEllipse(Position position, double radius, double xscale, double yscale){
		double x0 = position.getX()*xscale;
		double y0 = position.getY()*yscale;

		radius = radius*(1-1e-6);

		Integer[] end = {(int) (radius*xscale+x0),(int) y0};
		ArrayList<Integer[]> pixels = new ArrayList<Integer[]>();
		int pow,j = 0;
		double stap,cur;
		for(j = 0;j < 3;j++){
			pow = 0b1<<j;
			stap = 2*Math.PI/pow;
			for(int i = 0; i < pow;i++){
				cur = stap/2+i*stap;
				Integer[] p = {(int) Math.floor(Math.cos(cur)*radius*xscale + x0),(int) Math.floor(Math.sin(cur)*radius*yscale + y0)};
				pixels.add(p);
			}
		}

		pixels.add(end);

		while(!World.check(pixels)){
			//			System.out.println("------------");

			pixels.remove(pixels.size()-1);

			pow = 0b1<<j;
			stap = 2*Math.PI/pow;
			for(int i = 0; i < pow;i++){
				cur = stap/2+i*stap;
				Integer[] p = {(int) Math.floor(Math.cos(cur)*radius*xscale + x0),(int) Math.floor(Math.sin(cur)*radius*yscale + y0)};
				pixels.add(p);
			}

			pixels.add(end);
			j++;
		}

		ArrayList<Integer> circley = new ArrayList<Integer>();
		ArrayList<Integer[]> circlex = new ArrayList<Integer[]>();
		for(Integer[] p : pixels){
			int y = circley.indexOf(p[1]);
			if(y == -1){
				circley.add(p[1]);
				circlex.add(new Integer[]{p[0],p[0]});
			} else{
				Integer[] x = circlex.get(y);
				if(p[0] < x[0])
					x[0] = p[0];
				else if(p[0] > x[1])
					x[1] = p[0];
			}

		}
		ArrayList[] result = {circley,circlex};
		return result;

	}

	/**
	 * Checks if the circle is full by checking that each point is at most 1 step 
	 * removed from the last in both x and y; and that the last point is next to the
	 * first one
	 * 
	 * @param list The list to check, must be in binary tree format
	 * 
	 * @return whether the circle is full
	 */
	private static boolean check(ArrayList<Integer[]> list){
		int size = list.size();
		int current = size - 1;

		Integer[] c = list.get(current);
		Integer[] p = list.get(size -2);
		while(current != size -2){
			//			System.out.printf("p:%d:%d|c:%d:%d\n",p[0],p[1],c[0],c[1]);

			if(p[0]-c[0] < -1 || 1 < p[0]-c[0] || p[1]-c[1] < -1 || 1 < p[1]-c[1])
				return false;
			current = World.getNext(current,size);
			p = c;
			c = list.get(current);
		}
		return true;
	}

	/**
	 * Gets the next node in the binary tree from small to large
	 * 
	 * @param i the node we are at
	 * @param size the size of the tree
	 * 
	 * @return the next node
	 */
	private static int getNext(int i,int size){
		//get most left
		if(i*2+2 < size){
			i = i*2+2;
			while(i*2+1 < size)
				i = i*2+1;
			return i;
		} else {
			while(i != 0 && i == ((i-1)/2)*2+2){
				i = (i-1)/2;
			}
			return (i-1)/2;
		}

	}	
	
	/**
	 * Search for the next game object in a direct line in the direction we are facing.
	 */
	public Entity searchObject(Worm worm, double exp){
		Position A = worm.getPosition();
		double dir = worm.getDirection()+exp;
		Position B = new Position(A.getX() + Math.cos(dir), A.getY() + Math.sin(dir));
		
		Food closestFood = closestCollidingObject(this.getFoods(), A, B, Double.MAX_VALUE);		
		Worm closestWorm = closestCollidingObject(this.getWorms(), A, B, closestFood == null ? Double.MAX_VALUE : ModelUtil.distanceBetweenPositions(closestFood.getPosition(), A));
	
		return (closestWorm == null || closestWorm == worm) ? closestFood : closestWorm;
	}
	
	/**
	 * Find in the given collection the entity that lies on the line defined by l1 and l2 and is closest to l1. The maximum distance
	 * between the result and l1 should not exeed maxDistance.
	 * 
	 * @param objects: the objects to search through
	 * @param l1: the position where the result should lie closest to
	 * @param l2: defines with l1 a line
	 * @param maxDistance: the distance the result position and l1 should not exeed
	 * 
	 * @return: the entity that lies the closest to l1, lies on the line l1 and l2 and the distance between this entity and l1 does not exeed maxDistance,
	 * 			or null.
	 */
	private <T extends Entity> T closestCollidingObject(Iterable<T> objects, Position l1, Position l2, double maxDistance){
		T returnEntity = null;
		
		for(T nextEntity: objects){
			if(	(pointToLineDistance(l1, l2, nextEntity.getPosition()) <= nextEntity.getRadius()) &&
				( (returnEntity == null) || 
				  ( (ModelUtil.distanceBetweenPositions(l1, nextEntity.getPosition()) < maxDistance) && 
				    (ModelUtil.distanceBetweenPositions(l1, nextEntity.getPosition()) > ModelUtil.distanceBetweenPositions(l1, returnEntity.getPosition())) ) ) ){
					returnEntity = nextEntity;
				
			}
		}
		
		return returnEntity;
	}
	
	//Code snippet found at http://www.ahristov.com/tutorial/geometry-games/point-line-distance.html
	private double pointToLineDistance(Position A, Position B, Position P) {
	    double normalLength = Math.sqrt((B.getX()-A.getX())*(B.getX()-A.getX())+(B.getY()-A.getY())*(B.getY()-A.getY()));
	    return Math.abs((P.getX()-A.getX())*(B.getY()-A.getY())-(P.getY()-A.getY())*(B.getX()-A.getX()))/normalLength;
	}
}
