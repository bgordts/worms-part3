package worms.model;

/**
 * An exception class for when an entity cannot shoot
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class IllegalShootException  extends RuntimeException{

	private static final long serialVersionUID = -3680159160433949878L;

	public IllegalShootException(String message) {
		super(message);
	}

}
