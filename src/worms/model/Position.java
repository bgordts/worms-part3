package worms.model;

import worms.util.Util;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Represents a position in a 2D plane.
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Position implements Cloneable{
	
	//The coordinates
	private double x;
	private double y;

	/**
	 * Make a position
	 * 
	 * @param x the x-coordinate
	 * @param y the y-coordinate
	 * 
	 * @effect this.x is set to x
	 * 		 | this.setX(x)
	 * @effect this.y is set to y
	 * 		 | this.setY(y)
	 */
	protected Position(double x, double y) {
		this.setX(x);
		this.setY(y);
	}
	
	/** 
	 * Get the x position.
	 * 
	 * @return	the x position of this worm
	 * 		  | result == this.x
	 */
	@Basic
	public double getX(){
		return this.x;
	}
	
	/** 
	 * Get the y position
	 * 
	 * @return	the y position of this worm
	 * 		  | result == this.y
	 */
	@Basic
	public double getY(){
		return this.y;
	}
	
	/** 
	 * Set the x position of the worm
	 * 
	 * @param 	posititionX
	 * 			the position to set to
	 * @post	change the x position of the worm to positionX
	 * 		  | new.getPositionX() == positionX
	 * @throws	IllegalArgumentException
	 * 			the position must be a valid number
	 * 		  | !ModelUtil.isValidNumber(positionX)
	 * 			
	 */
	protected void setX(double positionX) throws IllegalArgumentException {
		if(!ModelUtil.isValidNumber(positionX))
			throw new IllegalArgumentException("The x position must be a valid number");
		this.x = positionX;
	}
	
	/** 
	 * Set the y position of the worm
	 * 
	 * @param 	posititionY
	 * 			the position to set to
	 * @post	change the Y position of the worm to positionY
	 * 		  | new.getPositionY() == positionY
	 * @throws	IllegalArgumentException
	 * 			the position must be a valid number
	 * 		  | !ModelUtil.isValidNumber(positionY)
	 * 			
	 */
	protected void setY(double positionY) throws IllegalArgumentException{
		if(!ModelUtil.isValidNumber(positionY))
			throw new IllegalArgumentException("The y position must be a valid number");
		this.y = positionY;
	}
	
	/**
	 * Print the coordinate in a string with format: "X: x Y: y"
	 * @return the string
	 */
	@Override
	public String toString(){
		return "X: " + this.getX() + " Y: " + this.getY();
	}
	
	/**
	 * Check if a position is equal to this position
	 * 
	 * @return whether it is equal
	 *	|	if(position instanceof Position)
	 *	|		Position castedPsition = (Position) position
	 *	|		result == Util.fuzzyEquals(this.getX(), castedPsition.getX()) && Util.fuzzyEquals(this.getY(), castedPsition.getY())
	 *	|	else
	 *	|		result ==  false
	 *	|
	 */
	@Override
	public boolean equals(Object position){
		if(position instanceof Position){
			Position castedPsition = (Position) position;
			return Util.fuzzyEquals(this.getX(), castedPsition.getX()) && Util.fuzzyEquals(this.getY(), castedPsition.getY());
		} else {
			return false;
		}		
	}
	
	/**
	 * Returns a clone of this object: a new Position with the same
	 * x and y coordinate.
	 * 
	 * @return	a new Position object with the same x and y coordinate as this object
	 * 		  |	result.getX() == this.getX() && result.getY() == this.getY()
	 */
	@Override
	public Position clone(){
		return new Position(this.getX(), this.getY());
	}

}
