package worms.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Model;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Most basic object a game world can contain. It has a position and a radius.
 * 
 * @invar 	The radius must be valid if the entity is active
 *		  | if(entity.isActive())
 * 		  |		isValidRadius(getRadius()) == true
 * @invar 	The position must be valid if the entity is active
 * 		  | if(entity.isActive())
 * 		  |		isValidPosition(getPosition()) == true
 * @invar 	The World may not be null, and this Entity must be part of the world if the entity is active
 * 		  | if(entity.isActive())
 * 		  |		getWorld() != null
 * 		  |		this.getWorld().containsEntity(this) == true
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 */
public class Entity {
	
	private World world;
	
	private Position position;
	
	private double radius;
	
	private boolean isActive;

	/**
	 * Constructor for Entity.
	 * 
	 * @param 	x
	 * 			the x value to initialize to
	 * @param 	y
	 * 			the y value to initialize to
	 * @param 	radius
	 * 			the radius to initialize to
	 * @param 	world
	 * 			the world where this entity lives in
	 * 
	 * @effect 	the world will be set to world
	 * 		  | this.setWorld(world)
	 * @effect	the radius will be set to radius
	 * 		  | this.setRadius(radius)
	 * @effect	the X and Y position will be set to x y
	 * 		  | this.setPosition(new Position(x, y))
	 * @effect	make this entity alive
	 * 		  | this.setIsActive(true)
	 */
	@Raw
	protected Entity(double x, double y, double radius, World world){
		this.setIsActive(true);
		this.setWorld(world);
		this.setRadius(radius);
		this.setPosition(new Position(x, y));
	}

	/**
	 * Remove this entity from the world.
	 * 
     * @effect   the entity is no longer active
     * 		  |  new.isActive() == false
     * @effect   the world this entity was initialized to does not longer contain this entity
     * 		  |  this.getWorld().containsEntity(this) == false
     * @effect   the entity is not longer part of the world it was initialized to
     * 		  |  new.getWorld() == null
	 */
	protected void destroy(){
		this.setIsActive(false);
		this.getWorld().removeEntity(this);
		this.setWorld(null);
	}
	
	/**
	 * Get the world this entity lives in.
	 * 
	 * @note	This method is annotated @Raw, because the radius must not be valid to check the World.	
	 * 
	 * @return 	the world this entity lives in
	 * 		  |	result == this.world
	 */
	@Basic @Immutable
	public World getWorld(){
		return this.world;
	}

	/**
	 * Returns a clone of the position of the entity.
	 * 
	 * @return	a clone of the position of this entity.
	 *        | result == this.position.clone()
	 */
	@Basic
	public Position getPosition() {
		return this.position.clone();
	}
	
	/**
	 * Get the current radius of the entity.
	 * 
	 * @return	the current radius of this entity
	 * 		  | result == this.radius
	 */
	@Basic
	public double getRadius() {
		return this.radius;
	}
	
	/**
	 * Get the current status of the entity.
	 * 
	 * @return 	the state of this entity. True if the entity is active.
	 * 		  | result == this.isActive
	 */
	@Basic
	protected boolean isActive(){
		return this.isActive;
	}
	
	/**
	 * Check if a given radius is valid.
	 * 
	 * @param 	radius
	 * 			radius to check
	 * 
	 * @note	this method is not static, so subclasses are able to use object-specific parameters to
	 * 			check the validity of the radius.
	 * 
	 * @return	returns true if all the conditions for a valid radius are met: it must be a valid double, it 
	 * 			may not be infinite and it must be greater than the lower bound of the radius.
	 * 		  | result == (not Double.isNaN(radius)) && (radius >= 0) && (not Double.isInfinite(radius))
	 */
	public boolean isValidRadius(double radius){
		return !Double.isNaN(radius) && radius >= 0 && !Double.isInfinite(radius);
	}
	
	/**
	 * Check if a given position is valid.
	 * 
	 * @param 	position
	 * 			position to check
	 * 
	 * @return	returns true if position is a valid position.
	 * 		  | result == this.getWorld().isInWorld(position)
	 */
	protected boolean isValidPosition(Position position){
		return this.getWorld().isInWorld(position,this.getRadius());
	}
	
	/**
	 * Set the world this Worm lives in.
	 * 
	 * @note	this method is annotated raw, because it is also used to set the world to null on destroy
	 * 
	 * @param 	world
	 * 			the world to set to
	 * 
	 * @post	The entity will be set in the given World.
	 * 		  | new.world == world
	 * @post	The entity will be added to the given world, if this world is not null.
	 * 		  | if(new.getWorld() != null)
	 * 		  |		new.getWorld().addEntity(this);
	 */
	@Raw @Model
	private void setWorld(World world){
		this.world = world;
		
		if(this.getWorld() != null){
			this.getWorld().addEntity(this);
		}
	}

	/**
	 * Set the position of this Entity.
	 * 
	 * @Note	The Entity is not removed from the world if the given position is invalid, because
	 * 			this method is written defensively.
	 * 
	 * @param	position
	 * 			The new position.
	 * 
	 * @post   	If the given position is valid, set the position to the given
	 * 		 | 	new.getPosition() == position
	 * 
	 * @throws	IllegalPositionException
	 *         	The given position is not valid.
	 *       | 	!isValidPosition(position) == false
	 */
	@Raw
	protected void setPosition(Position position) throws IllegalPositionException{
		if(!this.isValidPosition(position))
			throw new IllegalPositionException(this,"Not a valid position x:" + position.getX() + " y: " + position.getY());
		
		this.position = position.clone();
	}

	/**
	 * Set the current radius of the Entity.
	 * 
	 * @param 	radius
	 * 			The radius to set to.
	 * 
	 * @post	the radius of this worm is set to radius
	 * 		  | this.radius == radius
	 * 
	 * @throws	IllegalArgumentException
	 * 			the radius must be valid
	 * 		  | !isValidRadius(radius) == true
	 * 			
	 */
	@Raw
	protected void setRadius(double radius) throws IllegalArgumentException {
		if(!isValidRadius(radius))
			throw new IllegalArgumentException("The given radius is not valid");
		this.radius = radius;
	}

	/**
	 * Set the current radius of the Entity.
	 * 
	 * @param 	isActive
	 * 			The state to set to.
	 * 
	 * @post	the state of the Entity is set to the given state
	 * 		  | this.isActive == isActive
	 * 			
	 */
	@Raw
	private void setIsActive(boolean isActive){
		this.isActive = isActive;
	}
}
