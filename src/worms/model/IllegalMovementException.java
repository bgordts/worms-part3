package worms.model;

/**
 * An exception class for when an entity would make an illegal movement
 * 
 * @author Boris Gordts (2de bach Ir. Wet. CW-ELT), Jan Scheers (2de bach Ir. Wet. CW-ELT)
 * Repository: https://bitbucket.org/bgordts/worms-part3/overview
 *
 */
public class IllegalMovementException extends RuntimeException{

	private static final long serialVersionUID = -3644650050433949878L;

	public IllegalMovementException(String message) {
		super(message);
	}

}
