// Worm that prints the distance to the nearest other worm, turns and fires.

double angle;
entity nearestWorm;
double distanceToNearestWorm;
double x;
double y;
double r;
double wx;
double wy;
double wr;
double dir;
double tan;
double atan;
double distance;
entity w;
double PI;

PI := 3.14159265359;
while(true){
	x := getx self;
	y := gety self;
	r := getradius self;
	nearestWorm := null;
	foreach(worm, w) do {
	    if (w != self) {
	        wx := getx w;
  	      	wy := gety w;
  	      	wr := getradius w;
	        distance := sqrt((((x - wx) * (x - wx)) + ((y - wy) * (y - wy))));
	        if(nearestWorm == null) then {
	            nearestWorm := w;
	            distanceToNearestWorm := distance;
	        } else {
	            if(distance < distanceToNearestWorm) then {
	                nearestWorm := w;
	                distanceToNearestWorm := distance;
	            } 
	        }
	    }
	}
	if(nearestWorm != null) then {
	
		wx := ((getx nearestWorm) - x);
		wy := ((gety nearestWorm) - y);
		tan := (wy/wx);
		if((tan > 1) || (tan < (0 - 1))){
		    tan := (1/tan);
		}
		
		atan := (tan - ((tan*tan*tan)/3) + ((tan*tan*tan*tan*tan)/5) - ((tan*tan*tan*tan*tan*tan*tan)/7) + ((tan*tan*tan*tan*tan*tan*tan*tan*tan)/9));
		
		tan := (wy/wx);
		if((tan > 1) || (tan < (0 - 1))){
		    atan := (PI/2 - atan);
		}
		
		if((wx+wy) < 0){
		    atan := PI + atan;
		}
		
		dir := getdir self;
		angle := (atan - dir);
		if(angle > PI){
			angle := (angle - (2*PI));
		}
		if(angle < (0 - PI)){
			angle := (angle + (2*PI));
		}
		turn (angle);
	}
	toggleweap;
	if (! sameteam(nearestWorm)) {
		fire 100;
	}
	move;
	move;
	turn (PI/2);
	move;
}

