package worms.model;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import worms.model.entities.weapons.Bazooka;
import worms.model.entities.weapons.Rifle;
import worms.model.util.TestUtil;
import worms.util.Util;

public class WormTest {
	
	private static final double EPS = Util.DEFAULT_EPSILON;
	
	World worldFullPassable;
	Worm worm;
	Projectile projectile;
	
	World worldWithFloor;
	Worm worm2;
	Worm worm3;

	World worldWithHole;
	Worm worm4;

	Facade facade;
	World worldForShooting;
	Worm worm5;

	@Before
	public void setup() {
		worldFullPassable = new World(50.0, 50.0, TestUtil.passableMapCreatorAllPassable(50, 50), new Random());
		worm = new Worm(25, 25, Math.PI/2, 5, "TestWorm", worldFullPassable);
		//projectile = Projectile.createProjectile(worm, 7800, 50, worm.getHitPointsMax());
		
		worldWithFloor = new World(10, 10, TestUtil.passableMapWithFloor(10, 10), new Random());
		worm2 = new Worm(1, 6, Math.PI/2, 1, "TestWorm2", worldWithFloor);
		//Let the worm fall on the floor
		worm2.fall();
		
		worm3 = new Worm(7, 6, Math.PI, 1, "TestWorm3", worldWithFloor);
		worm3.fall();
		
		
		worldWithHole = new World(3, 3, TestUtil.passableMapOneHole(), new Random());
		worm4 =new Worm(2, 2, Math.PI/2, 1, "TestWorm4", worldWithHole);

		facade = new Facade();
		worldForShooting = facade.createWorld(10, 10, TestUtil.passableMapWithFloor(10, 10), new Random());
		Worm wormt = facade.createWorm(worldForShooting, 1, 6, Math.PI/2, 1, "TestWormTemp", null);
		wormt.fall();
		
		worm5 = facade.createWorm(worldForShooting, 7, 6, Math.PI, 1, "TestWorm5", null);
		worm5.fall();
		
		facade.startGame(worldForShooting);
		projectile = Projectile.createProjectile(wormt, 7800, 50, wormt.getHitPointsMax());
	}
	
	@Test
	public void constructorSmall_LegalCase() {
		Worm  worm = new Worm(25,25, Math.PI, 0.50, "Legit Worm", worldFullPassable);
		
		assertEquals(worm.getPosition().getX(), 25, TestUtil.EPS);
		assertEquals(worm.getPosition().getY(), 25, TestUtil.EPS);
		assertEquals(worm.getDirection(), Math.PI, TestUtil.EPS);
		assertEquals(worm.getRadius(), 0.50, TestUtil.EPS);
		assertEquals(worm.getRadiusLowerBound(), 0.25, TestUtil.EPS);
		assertEquals(worm.getName(), "Legit Worm");
		assertEquals(worm.getWorld(), worldFullPassable);
		assertEquals(worm.getActionPointsCurrent(), worm.getActionPointsMax());
		assertEquals(worm.getHitPointsCurrent(), worm.getHitPointsMax());
		assertEquals(worm.getHitPointsMax(), worm.getActionPointsMax());
	}
	
	@Test(expected = IllegalPositionException.class)
	public void constructorSmall_IllegalPositionCase() {
		Worm  worm = new Worm(1000,1000, Math.PI, 0.50, "Not legit Worm", worldFullPassable);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructorSmall_IllegalRadiusCase() {
		Worm  worm = new Worm(25,25, Math.PI, 0.001, "Not legit Worm", worldFullPassable);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructorSmall_IllegalNameCase() {
		Worm  worm = new Worm(25,25, Math.PI, 0.001, "122323 Not legit Worm", worldFullPassable);
	}
	
	@Test
	public void getRadiusLowerBound_SingleCase(){
		assertEquals(worm.getRadiusLowerBound(), 0.25, TestUtil.EPS);
	}
	
	@Test
	public void getName_SingleCase(){
		assertEquals(worm.getName(), "TestWorm");
	}
	
	@Test
	public void getActionPointsMax_SingleCase(){
		assertEquals(worm.getName(), "TestWorm");
	}
	
	@Test
	public void getHitPointsMax_SingleCase(){
		assertEquals(556062, worm.getHitPointsMax());
	}
	
	@Test
	public void getActionPointsCurrent_SingleCase(){
		assertEquals(worm.getActionPointsCurrent(), 556062);
	}
	
	@Test
	public void getHitPointsCurrent_SingleCase(){
		assertEquals(worm.getHitPointsCurrent(), 556062);
	}
	
	@Test
	public void getMass_SingleCase(){
		assertEquals(worm.getMass(), 556061.8996853934, TestUtil.EPS);
	}
	
	@Test
	public void getJumpForce_SingleCase(){
		assertEquals(worm.getJumpForce(), 8233414.428549763, TestUtil.EPS);
	}
	
	@Test
	public void getSelectedWeapon_SingleCase(){
		assertTrue(worm.getSelectedWeapon() instanceof Rifle || worm.getSelectedWeapon() instanceof Bazooka);
	}
	
	@Test
	public void getTeam_SingleCase(){
		assertTrue(worm.getTeam() == null);
		Team team = new Team("NewTeam");
		worm.setTeam(team);
		assertTrue(worm.getTeam().equals(team));
	}
	
	@Test
	public void isValidRadius_TrueCase(){
		assertTrue(worm.isValidRadius(60));
	}
	
	@Test
	public void isValidRadius_NANFalseCase(){
		assertFalse(worm.isValidRadius(Double.NaN));
	}
	
	@Test
	public void isValidRadius_InfinteFalseCase(){
		assertFalse(worm.isValidRadius(Double.POSITIVE_INFINITY));
		assertFalse(worm.isValidRadius(Double.NEGATIVE_INFINITY));
	}
	
	@Test
	public void isValidRadius_LowerBoundFalseCase(){
		assertFalse(worm.isValidRadius(0.24));
	}
	
	@Test
	public void canJump_TrueCase(){
		assertTrue(worm.canJump(0.005));
	}
	
	@Test
	public void canJump_TooCloseFalseCase(){
		assertFalse(worm2.canJump(0.005));
	}
	
	@Test
	public void canJump_NoActionPointsFalseCase(){
		worm.setActionPointsCurrent(0);
		assertFalse(worm.canJump(0.005));
	}
	
	@Test
	public void canMove_TrueCase(){
		assertTrue(worm3.canMove());
	}
	
	@Test
	public void canMove_NoActionPointsFalseCase(){
		worm3.setActionPointsCurrent(0);
		assertFalse(worm3.canMove());
	}
	
	@Test
	public void canMove_NoActionPointsVerticalFalseCase(){
		worm2.setActionPointsCurrent(2);
		assertFalse(worm2.canMove());
	}
	
	@Test
	public void canMove_CanNotMoveFarEnoughCase(){
		assertFalse(worm4.canMove());
	}
	
	@Test
	public void canFall_TrueCase(){
		worm3.setPosition(new Position(7, 7));
		assertTrue(worm3.canFall());
	}
	
	@Test
	public void canFall_NoRoomToFallFalseCase(){
		assertFalse(worm3.canFall());
	}
	
	@Test
	public void canTurn_TrueCase() {
		assertTrue(worm3.canTurn(Math.PI));
	}
	
	@Test
	public void canTurn_LimitTrueCase() {
		worm3.setActionPointsCurrent(60);
		assertTrue(worm3.canTurn(2*Math.PI));
	}
	
	@Test
	public void canTurn_LimitFalseCase() {
		worm3.setActionPointsCurrent(60);
		assertFalse(worm3.canTurn(2*Math.PI + Math.PI/30));
	}
	
	@Test
	public void canTurn_NaNFalseCase() {
		assertFalse(worm3.canTurn(Double.NaN));
	}
	
	@Test
	public void canShoot_TrueCase() {
		assertTrue(worm.canShoot());
	}
	
	@Test
	public void canShoot_NotEnoughActionPointsCase() {
		worm.setActionPointsCurrent(1);
		assertFalse(worm.canShoot());
	}
	
	@Test
	public void move_StraightLineHorizontalCase() {
		worm3.setPosition(new Position(7, 2));
		Position pos1 = worm3.getPosition();
		worm3.move();
		Position pos2 = worm3.getPosition();
		
		assertEquals(1, ModelUtil.distanceBetweenPositions(pos1, pos2), TestUtil.EPS);
		assertEquals(new Position(6, 2), worm3.getPosition());
	}
	
	@Test
	public void move_StraightLineVerticalCase() {
		worm3.setDirection(Math.PI/2);
		worm3.setPosition(new Position(7, 2));
		
		Position pos1 = worm3.getPosition();
		worm3.move();
		Position pos2 = worm3.getPosition();
		
		assertEquals(1, ModelUtil.distanceBetweenPositions(pos1, pos2), TestUtil.EPS);
		assertEquals(new Position(7, 3), worm3.getPosition());
	}
	
//	/**
//	 * When the worm wants to move over 45 degrees with the terrain, it should move along the slope of the terrain.
//	 */
//	@Test
//	public void move_45DegreesHorizontalCase() {
//		worm3.setDirection((3/4)*Math.PI);
//		worm3.setPosition(new Position(7, 2));
//		
//		Position pos1 = worm3.getPosition();
//		worm3.move();
//		Position pos2 = worm3.getPosition();
//		
//		assertEquals(1, ModelUtil.distanceBetweenPositions(pos1, pos2), TestUtil.EPS);
//		assertEquals(new Position(6, 2), worm3.getPosition());
//	}
	
	@Test(expected = IllegalMovementException.class)
	public void move_NoActionPointsFalseCase(){
		worm3.setActionPointsCurrent(0);
		worm3.move();
	}
	
	@Test(expected = IllegalMovementException.class)
	public void move_CanNotMoveFarEnoughCase(){
		worm4.move();
	}
	
	@Test
	public void fall_LegalCase() {
		worm3.setPosition(new Position(7, 5));
		
		Position pos1 = worm3.getPosition();
		worm3.fall();
		Position pos2 = worm3.getPosition();
		
		assertEquals(new Position(7, 2), worm3.getPosition());
	}
	
	@Test(expected = IllegalMovementException.class)
	public void fall_IlLegalCase() {
		worm3.setPosition(new Position(7, 2));
		
		Position pos1 = worm3.getPosition();
		worm3.fall();		
	}
	
	@Test
	public void eat_EatPossibleCase() {
		Food food = new Food(worm3.getPosition().getX(), worm3.getPosition().getY(), worldWithFloor);
		
		worm3.setRadius(100);
		worm3.eat();
		double radiusAfter = worm3.getRadius();
		
		assertEquals(110, radiusAfter, TestUtil.EPS);
	}
	
	@Test
	public void eat_EatNotPossibleCase() {
		Food food = new Food(worm3.getPosition().getX()-5, worm3.getPosition().getY(), worldWithFloor);
		
		worm3.setRadius(1);
		worm3.eat();
		double radiusAfter = worm3.getRadius();
		
		assertEquals(1, radiusAfter, TestUtil.EPS);
	}
	
	@Test
	public void eat_EatMultipleCase() {
		Food food = new Food(worm3.getPosition().getX(), worm3.getPosition().getY(), worldWithFloor);
		
		Food food2 = new Food(worm3.getPosition().getX() + 0.01, worm3.getPosition().getY(), worldWithFloor);
		
		worm3.setRadius(1);
		worm3.eat();
		double radiusAfter = worm3.getRadius();
		
		assertEquals(1*1.1*1.1, radiusAfter, TestUtil.EPS);
	}
	
	@Test
	public void turn_180DegreesCase() {
		worm.setDirection(0);
		
		int actionPointsBeforeMove = worm.getActionPointsCurrent();
		
		worm.turn(Math.PI);
		
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI));
		
		assertTrue(worm.getActionPointsCurrent() == actionPointsBeforeMove - 30);
	}
	
	@Test
	public void turn_269DegreesCase() {
		worm.setDirection(0);
		
		int actionPointsBeforeMove = worm.getActionPointsCurrent();
		
		//269 degrees = 4.694935688
		worm.turn(4.694935688);
		
		assertTrue(Util.fuzzyEquals(worm.getDirection(), 4.694935688));
		
		//4.694935688*(60/(2*Pi)) = 45
		assertTrue(worm.getActionPointsCurrent() == actionPointsBeforeMove - 45);
	}
	
	@Test
	public void turn_180DegreesFrom90DegreesCase() {
		worm.setDirection(Math.PI/2);
		
		int actionPointsBeforeMove = worm.getActionPointsCurrent();
		
		worm.turn(Math.PI);
		
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI + Math.PI/2));
		
		assertTrue(worm.getActionPointsCurrent() == actionPointsBeforeMove - 30);
	}
	
	@Test
	public void turn_negativeCase() {
		worm.setDirection(0);
		int actionPointsBeforeMove = worm.getActionPointsCurrent();
		
		worm.turn(-Math.PI);
		
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI));
		
		assertTrue(worm.getActionPointsCurrent() == actionPointsBeforeMove - 30);
	}
	
	@Test
	public void turn_540DegreesCase() {
		worm.setDirection(0);
		int actionPointsBeforeMove = worm.getActionPointsCurrent();
		
		worm.turn(Math.PI*3);
		
		assertTrue(Util.fuzzyEquals(worm.getDirection(), Math.PI));
		
		assertTrue(worm.getActionPointsCurrent() == actionPointsBeforeMove - 90);
	}
	
	@Test(expected = IllegalMovementException.class)
	public void jump_VerticalIllegalCase() {
		worm3.setDirection(Math.PI/2);
		
		worm3.jump(0.0005);
	}
	
	@Test
	public void jump_45DegreesCase() {
		worm3.setPosition(new Position(2, 2));
		worm3.setDirection(Math.PI/4);
		
		worm3.jump(0.0001);
		
		System.out.println("pos: " + worm3.getPosition());
		
		assertEquals(new Position(7.588041478, 2.000523423), worm3.getPosition());		
		
		assertEquals(0, worm3.getActionPointsCurrent());
	}
	
	@Test
	public void jump_jumpAgainstWallCase() {
		worm3.setPosition(new Position(5, 2));
		worm3.setDirection(Math.PI/3);
		
		worm3.jump(0.0001);
		
		System.out.println("pos: " + worm3.getPosition());
		
		assertEquals(new Position(7.999629848, 3.975443432), worm3.getPosition());	
		
		assertEquals(0, worm3.getActionPointsCurrent());
	}
	
	@Test
	public void jump_WithFoodLegalCase() {
		Food food = new Food(8,4, worldWithFloor);
		
		assertTrue(food.isActive());
		
		worm3.setPosition(new Position(5, 2));
		worm3.setDirection(Math.PI/3);
				
		worm3.jump(0.0001);
		
		System.out.println("pos: " + worm3.getPosition());
		
		assertEquals(new Position(7.999629848, 3.975443432), worm3.getPosition());	
		
		assertEquals(0, worm3.getActionPointsCurrent());
		
		assertFalse(food.isActive());
	}
	
	@Test
	public void shoot_SingleCase() {
		int actionPointsBefore = worm5.getActionPointsCurrent();
		
		worm5.shoot(50);
		
		int actionPointsAfter = worm5.getActionPointsCurrent();
		
		assertEquals(actionPointsBefore - worm5.getSelectedWeapon().getActionPointCost(), actionPointsAfter);
	}
	
	@Test
	public void playTurn_SingleCase() {
		worm.setActionPointsCurrent(5);
		worm.setHitPointsCurrent(10);
		
		worm.playTurn();
		
		assertEquals(worm.getActionPointsCurrent(), worm.getActionPointsMax());
		assertEquals(worm.getHitPointsCurrent(), 20);
	}
	
	@Test
	public void selectNextWeapon_StandardWeaponsCase() {
		assertTrue(worm.getSelectedWeapon() instanceof Rifle);
		
		worm.selectNextWeapon();
		
		assertTrue(worm.getSelectedWeapon() instanceof Bazooka);
		
		worm.selectNextWeapon();
		
		assertTrue(worm.getSelectedWeapon() instanceof Rifle);
	}
	
	@Test
	public void setName_legalSimpleCase(){
		worm.setName("Jonas");
		
		assertTrue(worm.getName() == "Jonas");
	}
	
	@Test
	public void setName_legalQuoteCase(){
		worm.setName("James o'Hara");
		
		assertTrue(worm.getName() == "James o'Hara");
	}
	
	public void setName_DigitCase(){
		worm.setName("James o'Hara 007");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalNoFirstCapitalCase(){
		worm.setName("jonas");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalTooShortCase(){
		worm.setName("U");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalSpecialCharCase(){
		worm.setName("Boris&Jan");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setName_illegalNullCase(){
		worm.setName(null);
	}
	
	@Test
	public void setActionPointsCurrent_simpleCase(){
		worm.setRadius(50);
		worm.setActionPointsCurrent(50);
		
		assertTrue(worm.getActionPointsCurrent() == 50);
	}
	
	@Test
	public void setActionPointsCurrent_negativeCase(){
		worm.setActionPointsCurrent(-50);
		
		assertTrue(worm.getActionPointsCurrent() == 0);
	}
	
	@Test
	public void setActionPointsCurrent_tooHighCase(){
		worm.setActionPointsCurrent(worm.getActionPointsMax() + 1000);
		
		assertTrue(worm.getActionPointsCurrent() == worm.getActionPointsMax());
	}
	
	@Test
	public void setHitPointsCurrent_simpleCase(){
		worm.setRadius(50);
		worm.setHitPointsCurrent(50);
		
		assertTrue(worm.getHitPointsCurrent() == 50);
	}
	
	@Test
	public void setHitPointsCurrent_negativeCase(){
		worm.setHitPointsCurrent(-50);
		
		assertTrue(worm.getHitPointsCurrent() == 0);
		assertFalse(worm.isActive());
	}
	
	@Test
	public void setHitPointsCurrent_tooHighCase(){
		worm.setHitPointsCurrent(worm.getHitPointsMax() + 1000);
		
		assertTrue(worm.getHitPointsCurrent() == worm.getHitPointsMax());
	}
	
	@Test
	public void setRadius_simpleCase(){
		worm.setRadius(0.30);
		
		assertTrue(worm.getRadius() == 0.30);
	}
	
	@Test
	public void setRadius_lessActionPointsCase(){
		worm.setRadius(0.26);
		
		assertTrue(worm.getRadius() == 0.26);
		assertTrue(worm.getActionPointsCurrent() == worm.getActionPointsMax());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setRadius_illegalNaNCase(){
		worm.setRadius(Math.sqrt(-1));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setRadius_illegalTooSmallCase(){
		worm.setRadius(worm.getRadiusLowerBound() - 0.02);
	}
	
	@Test
	public void setTeam_LegitCase(){
		Team t = new Team("TeamBlue");
		worm.setTeam(t);
		assertEquals(t, worm.getTeam());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setTeam_IllegalCase(){
		Team t = new Team("Team Blue");
	}
	
	@Test
	public void setHitPointsCurrent_50Case() {
		worm.setHitPointsCurrent(worm.getHitPointsCurrent() - 50);
	}
	
	/**
	 * Tests for JumpableEntity:
	 */
	@Test
	public void getDirectory_SingleCase() {
		assertEquals(Math.PI, worm3.getDirection(), TestUtil.EPS);
	}
	
	@Test
	public void isValidDirection_TrueCase() {
		assertTrue(JumpableEntity.isValidDirection(Math.PI*(3/2)));
	}
	
	@Test
	public void isValidDirection_NegativeFalseCase(){
		assertFalse(JumpableEntity.isValidDirection(-5));
	}
	
	@Test
	public void isValidDirection_TooBigFalseCase(){
		assertFalse(JumpableEntity.isValidDirection(50));
	}
	
	@Test
	public void isValidDirection_NaNFalseCase(){
		assertFalse(JumpableEntity.isValidDirection(Double.NaN));
	}
	
	@Test
	public void isValidJumpForce_TrueCase() {
		assertTrue(worm3.isValidJumpForce(500));
	}
	
	@Test
	public void isValidJumpForce_NegativeFalseCase() {
		assertFalse(worm3.isValidJumpForce(-5));
	}
	
	@Test
	public void isValidJumpForce_NaNFalseCase() {
		assertFalse(worm3.isValidJumpForce(Double.NaN));
	}
	
	@Test
	public void isValidTimeStep_TrueCase() {
		assertTrue(worm3.isValidTimeStep(0.00005));
	}
	
	@Test
	public void isValidTimeStep_NegativeFalseCase() {
		assertFalse(worm3.isValidTimeStep(-0.00005));
	}
	
	@Test
	public void isValidTimeStep_NanFalseCase() {
		assertFalse(worm3.isValidTimeStep(Double.NaN));
	}
	
	@Test
	public void jumpTime_45DegreesLegalCase() {
		worm3.setPosition(new Position(2, 2));
		worm3.setDirection(Math.PI/4);
		
		assertEquals(1.067590773, worm3.jumpTime(0.00001), 0.00001);
	}
	
	@Test
	public void jumpTime_ZeroJumptimeLegalCase() {
		worm3.setPosition(new Position(8, 2));
		worm3.setDirection(Math.PI/4);
		
		assertEquals(0, worm3.jumpTime(0.00001), 0.00001);
	}
	
	@Test
	public void isJumpablePosition_TrueCase() {
		assertTrue(worm3.isJumpablePosition(new Position(5, 5)));
	}
	
	@Test
	public void isJumpablePosition_True2Case() {
		assertTrue(worm3.isJumpablePosition(new Position(8, 2)));
	}
	
	@Test
	public void isJumpablePosition_True3Case() {
		assertTrue(worm3.isJumpablePosition(new Position(0, 2)));
	}
	
	@Test
	public void isJumpablePosition_OutOfWorldFalseCase() {
		assertFalse(worm3.isJumpablePosition(new Position(10, 2)));
	}
	
	@Test
	public void isJumpablePosition_WallFalseCase() {
		assertFalse(worm3.isJumpablePosition(new Position(9, 2)));
	}
	
	@Test
	public void jumpStep_45DegreesLegalCase() {
		worm3.setPosition(new Position(2, 2));
		worm3.setDirection(Math.PI/4);
		
		System.out.println("jump force: " + worm3.getJumpForce());
		System.out.println("mass: " + worm3.getMass());
		System.out.println("jump time: " + worm3.jumpTime(0.0001));
		
		assertEquals(new Position(7.234744524, 2.331419524), worm3.jumpStep(1));
	}
	
	@Test
	public void jumpStep_45Degrees2LegalCase() {
		worm3.setPosition(new Position(2, 2));
		worm3.setDirection(Math.PI/4);
		
		System.out.println("jump force: " + worm3.getJumpForce());
		System.out.println("mass: " + worm3.getMass());
		System.out.println("jump time: " + worm3.jumpTime(0.0001));
		
		assertEquals(new Position(263.7372262, -11994.57527), worm3.jumpStep(50));
	}
	
	@Test
	public void jumpStep_150DegreesLegalCase() {
		worm3.setPosition(new Position(2, 2));
		worm3.setDirection(2.617993878);
		
		System.out.println("jump force: " + worm3.getJumpForce());
		System.out.println("mass: " + worm3.getMass());
		System.out.println("jump time: " + worm3.jumpTime(0.0001));
		
		assertEquals(new Position(-318.5613256, -12071.23633), worm3.jumpStep(50));
	}
	
	@Test
	public void setDirection_LegalCase() {
		worm3.setDirection(4);
		
		assertEquals(4, worm3.getDirection(), TestUtil.EPS);
	}
	
	
	
	/**
	 * Tests for Entity:
	 */
	@Test
	public void getWorld_SingleCase() {
		assertEquals(worldWithFloor, worm3.getWorld());
	}
	
	@Test
	public void getPosition_SingleCase() {
		assertEquals(new Position(7, 2), worm3.getPosition());
	}
	
	@Test
	public void getRadius_SingleCase() {
		assertEquals(1, worm3.getRadius(), TestUtil.EPS);
	}
	
	@Test
	public void isActive_SingleCase() {
		assertTrue(worm3.isActive());
	}
	
	@Test
	public void destroy_SingleCase() {
		assertTrue(worm3.isActive());
		assertTrue(worm3.getWorld().getWorms().contains(worm3));
		
		worm3.destroy();
		
		assertFalse(worm3.isActive());
		assertFalse(worldWithFloor.getWorms().contains(worm3));
	}
	
	@Test
	public void setPosition_LegalCase() {
		worm3.setPosition(new Position(5, 5));
		
		assertEquals(new Position(5, 5), worm3.getPosition());
	}
	
	@Test(expected = IllegalPositionException.class)
	public void setPosition_OutOfTheWorldCase() {
		worm3.setPosition(new Position(-50, -5));
	}
	
	@Test
	public void setRadius_LegalCase() {
		worm3.setRadius(6);
		
		assertEquals(6, worm3.getRadius(), TestUtil.EPS);
	}

}
