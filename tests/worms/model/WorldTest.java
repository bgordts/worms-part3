package worms.model;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import worms.model.util.TestUtil;
import worms.util.Util;

public class WorldTest {
	
	private static final double EPS = Util.DEFAULT_EPSILON;
	private Random random;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		random = new Random(7357);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDimensionW() {
		new World(Double.NaN,1, null, random);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidDimensionH() {
		new World(1,-1,null,random);
	}
	
	@Test
	public void testConstructor(){
		new World(0,Double.MAX_VALUE,null,random);
	}
	
	@Test
	public void testNullGame(){
		World world = new World(1,1,null,random);
		world.startGame();
		world.nextTurn();
		assertTrue(world.isGameFinished());
		assertEquals(world.getWinner(), null);
		
	}
	
	@Test(expected = IllegalStateException.class)
	public void testNullSpawn(){
		World world = new World(1,1,null,random);
		world.spawnEntity('w', null);
		assertEquals(world.getWorms().size(), 1);
		
	}
	
	@Test(expected = IllegalStateException.class)
	public void testZeroWorld(){
		boolean[][] map = new boolean[][]{{true}};
		World world = new World(0,0,map,random);
		Position p = new Position(0,0);
		assertTrue(world.isPassable(p, 1));
		assertFalse(world.isAdjacent(p, 1));
		
		world.spawnEntity('w', null);
	}
	
	/* # # #
	 * # w #
	 * # # #
	 */
	@Test
	public void testSmallestWorld(){
		boolean[][] map = new boolean[][]{{true}};
		World world = new World(1,1,map,random);
		Worm worm = new Worm(.5,.5,0,.5,"Worm",world, null);
		
		Position n = world.getMaxMoveDistance(worm, new Position(1,1));
		assertEquals(n.getX(), 1,EPS);
		assertEquals(n.getY(), 1,EPS);
		
		n = world.getMaxFallDistance(worm);
		assertEquals(n.getX(), .5,EPS);
		assertTrue(n.getY() < 0);
		
	}
	
	/* # # # # #
	 * # X X X #
	 * # X w X #
	 * # X X X #
	 * # # # # #
	 */
	@Test
	public void testSmallBox(){
		boolean[][] map = new boolean[][]{	{false,false,false},
											{false,true ,false},
											{false,false,false}};
		World world = new World(3,3,map,random);
		//Worms may overlap
		new Worm(1.5,1.5,0,.5,"Worm",world);
		new Worm(1.5,1.5,0,.5,"Worm",world);
		try{
			new Worm(1.5,1.5,0,.6,"Worm",world);
		} catch(IllegalArgumentException ex){}
	}
	
	/*	# # # # #
	 *  # X X X #
	 *  # . . . #
	 *  # . . . #
	 *  # # # # #
	 * 
	 */
	@Test
	public void testAdjacent(){
		boolean[][] map = new boolean[][]{	{false,false,false},
											{true ,true ,true },
											{true ,true ,true }};
		World world = new World(3,3,map,random);
		Position p = new Position(1.5,1.5);
		
		assertTrue(world.isAdjacent(p, .5));
		p.setY(1.5 -.05+EPS);
		assertTrue(world.isAdjacent(p, .5));
		p.setY(1.5 -.05);
		assertFalse(world.isAdjacent(p, .5));
		
		
	}
	
	/*	# # # # #
	 *  # . . . #
	 *  # . X . #
	 *  # . . . #
	 *  # # # # #
	 * 
	 */
	@Test
	public void testPassable(){
		boolean[][] map = new boolean[][]{	{true ,true ,true },
											{true ,false,true },
											{true ,true ,true }};
		World world = new World(3,3,map,random);
		Position p = new Position(1.5,1.5);
		assertFalse(world.isPassable(p, 1));
		p = new Position(0,0);
		assertTrue(world.isPassable(p, Math.sqrt(2)));
		assertTrue(world.isAdjacent(p, Math.sqrt(2)));
		
		
	}
	
	@Test
	public void testAddAndRemove(){
		boolean[][] map = new boolean[][]{{true}};
		World world = new World(1,1,map,random);
		Worm worm = new Worm(.5,.5,0,.25,"Worm", world);
		try{
			world.addEntity(worm);
		} catch (IllegalArgumentException ex){}
		new Worm(.5,.5,0,.25,"Worm", world);
		new Worm(.5,.5,0,.25,"Worm", world);
		new Food(.5,.5,world);
		new Food(.5,.5,world);
		new Food(.5,.5,world);

		assertEquals(world.getWorms().size(),3);
		assertEquals(world.getFoods().size(),3);
		
		int i = 2;
		Iterable<Worm> worms = world.getWorms();
		for(Worm wormi : worms){
			world.removeEntity(wormi);
			assertEquals(world.getWorms().size(),i--);
			assertFalse(world.contains(wormi));
		}
		
		i = 2;
		HashSet<Food> foods = world.getFoods();
		for(Food food : foods){
			world.removeEntity(food);
			assertEquals(world.getFoods().size(),i--);
			assertFalse(world.contains(food));
		}
		
	}
	
	@Test
	public void testCycling(){
		boolean[][] map = new boolean[][]{{true}};
		World world = new World(1,1,map,random);
		new Worm(.5,.5,0,.25,"Worm1", world);
		new Worm(.5,.5,0,.25,"Worm2", world);
		new Worm(.5,.5,0,.25,"Worm3", world);
		new Worm(.5,.5,0,.25,"Worm4", world);

		for(int i = 0; i < 10; i++)
			world.addTeam("Team");
		
		try{world.addTeam("Too much");}
		catch(IllegalArgumentException ex){}
		
		world.startGame();

		try{world.spawnEntity('w', null);}
		catch(IllegalStateException ex){}
		
		try{world.spawnEntity('f', null);}
		catch(IllegalStateException ex){}
		
		for(int i = 0; i < 3; i++){
			Worm worm = world.getActiveWorm();
			world.removeEntity(worm);
			assertNotEquals(null,world.getActiveWorm());
			world.nextTurn();
			assertNotEquals(null,world.getActiveWorm());
		}
		assertTrue(world.isGameFinished());
		System.out.println(world.getWinner());
	}
	
	public void test_SearchObjectLegalCase(){
		World worldWithFloor = new World(10, 10, TestUtil.passableMapWithFloor(10, 10), new Random());
		
		Worm worm = new Worm(6, 6, 0, 1, "TestWorm", worldWithFloor);
		Worm worm2 = new Worm(8, 6, 0, 1, "TestWorm2", worldWithFloor);
		Worm worm3 = new Worm(3, 6, 0, 1, "TestWorm3", worldWithFloor);
		worm.fall();
		worm2.fall();
		worm3.fall();
		
		assertEquals(worm, worldWithFloor.searchObject(worm3, 0));
		assertEquals(worm3, worldWithFloor.searchObject(worm, Math.PI));
	}
	
	public void test_SearchObjectIllegalCase(){
		World worldWithFloor = new World(10, 10, TestUtil.passableMapWithFloor(10, 10), new Random());
		
		Worm worm = new Worm(6, 6, 0, 1, "TestWorm", worldWithFloor);
		Worm worm2 = new Worm(8, 6, 0, 1, "TestWorm2", worldWithFloor);
		Worm worm3 = new Worm(3, 6, 0, 1, "TestWorm3", worldWithFloor);
		worm.fall();
		worm2.fall();
		worm3.fall();
		
		assertNotEquals(worm2, worldWithFloor.searchObject(worm3, 0));
	}

}
