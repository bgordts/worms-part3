package worms.model.util;

import static org.junit.Assert.assertEquals;
import worms.model.Position;
import worms.util.Util;

public class TestUtil {
	
	public static final double EPS = Util.DEFAULT_EPSILON;

	public static boolean[][] passableMapCreatorAllPassable(int width, int height){
		boolean[][] returnMap = new boolean[width][height];
		
		for(int i = 0; i < width; i++){
			for(int y = 0; y < height; y++){
				returnMap[i][y] = true;
			}
		}
		
		return returnMap;
	}
	
	public static boolean[][] passableMapCreatorLeftColumnNotPassable(int width, int height){
		boolean[][] returnMap = new boolean[width][height];
		
		for(int i = 0; i < width; i++){
			for(int y = 0; y < height; y++){
				if(i == 0){
					returnMap[i][y] = false;
				} else {
					returnMap[i][y] = true;
				}
			}
		}
		
		return returnMap;
	}
	
	public static boolean[][] passableMapWithFloor(int width, int height){
		boolean[][] passableMap = new boolean[][] {
				{ true, true, true, true, true, true, true, true, true, false }, { true, true, true, true, true, true, true, true, true, false },
				{ true, true, true, true, true, true, true, true, true, false }, { true, true, true, true, true, true, true, true, true, false },
				{ true, true, true, true, true, true, true, true, true, false }, { true, true, true, true, true, true, true, true, true, false },
				{ true, true, true, true, true, true, true, true, true, false }, { true, true, true, true, true, true, true, true, true, false },
				{ true, true, true, true, true, true, true, true, true, false }, { false, false, false, false, false, false, false, false, false, false } };
		
		return passableMap;
	}
	
	public static boolean[][] passableMapOneHole(){
		boolean[][] passableMap = new boolean[][] {
				{ false, false, false}, { false, true, false}, { false, false, false}};
		
		return passableMap;
	}
	
	public static void assertEqualsPosition(Position a, Position b){
		assertEquals(a.getX(), b.getX(), EPS);
		assertEquals(a.getY(), b.getY(), EPS);
	}

}
